﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Keybord : MonoBehaviour {

	public int numberOfLetter;
	private int currentPosition;
	public Transform answerPanel;
	public string  word;
	private GameObject[] letterSlot;
	private bool[] bonusOpen;

	public QuestionTab questScript;
	public GameObject letterPrefab;
	public Sprite emptyLetter;

	public void CreateWord (string fromWord)
	{
		//fromWord = "123456 12345";
		int childCount = answerPanel.childCount;
		for (int i = 0; i < childCount; i++) {
			GameObject.Destroy(answerPanel.GetChild(i).gameObject);
		}

		word = fromWord;
		// Number of letters in a word
		numberOfLetter = NumOfLetters (word);
		bonusOpen = new bool[numberOfLetter];
		//Create a slots for each word
		letterSlot = new GameObject[numberOfLetter];
		for ( int i = 0; i < numberOfLetter; i++) {
			letterSlot [i] = (GameObject)  Instantiate (letterPrefab, Vector3.zero, Quaternion.identity) ;
			//letterSlot [i] = clone;
			letterSlot [i].transform.SetParent (answerPanel); 
			letterSlot [i].transform.localScale = Vector3.one;
			if (word [i].ToString () == " ") 
			{
				letterSlot [i].GetComponent<Image> ().sprite = emptyLetter;
				bonusOpen [i] = true;
				letterSlot [i].GetComponentInChildren<Text> ().text = " ";
			} else if (i != 0)
			{
				int toPos = i;
				letterSlot [i].GetComponent<Button>().onClick.AddListener(() => ChangeCurPosition(toPos));
			}
		}
		//Set with first letter from the word
		letterSlot[0].GetComponentInChildren<Text>().text = FirstLetter(word).ToString(); 
		OnSelected (1);
		currentPosition = 1;
	}

	public void ChangeCurPosition (int toPos)
	{
		Debug.Log ("changing to: " + toPos);
		OnDeselected (currentPosition);
		currentPosition = toPos;
		OnSelected (currentPosition);
	}

	//Method which add a letter in slot
	public void TextButton(){
		// Get a button name
		string letter = EventSystem.current.currentSelectedGameObject.name;

		OnDeselected (currentPosition);
		letterSlot [currentPosition].GetComponentInChildren<Text> ().text = letter;

		currentPosition++;
		if (currentPosition > numberOfLetter - 1)
			currentPosition = numberOfLetter - 1;
		
		while (bonusOpen [currentPosition]) 
		{
			if (currentPosition == numberOfLetter - 1)
				while (bonusOpen [currentPosition])
					currentPosition--;
			else
				currentPosition++;
		}

		OnSelected (currentPosition);


		if (AllFieldsNotEmpty())
		{
			//last letter input so check the answer
			CheckTheAnswer();
		}
	}

	bool AllFieldsNotEmpty ()
	{
		for (int i = 0; i < numberOfLetter; i++) 
		{
			if (letterSlot [i].GetComponentInChildren<Text> ().text == "")
				return false;
		}
		return true;
	}

	void CheckTheAnswer ()
	{
		string answer = "";
		for (int i = 0; i < numberOfLetter; i++) 
		{
			answer += letterSlot [i].GetComponentInChildren<Text> ().text;
		}
		Debug.Log ("check answer: " + answer);
		if (word == answer)
			questScript.AnswerQuestion (1);
		else //show indication about wrong answer
			questScript.AnswerQuestion (0);
	}

	//Method which delete a letter from slot
	public void BackSpace()
	{
		Debug.Log ("backspace pressed. cur pos: " + currentPosition.ToString());
		OnDeselected(currentPosition);
		currentPosition--;
		if (currentPosition == 0)
			currentPosition = 1;
		if (currentPosition > numberOfLetter - 1)
			currentPosition = numberOfLetter - 1;
		if (currentPosition > 0) 
		{
			Debug.Log ("cur pos > 0");
			while (bonusOpen [currentPosition] && currentPosition != 1) 
			{
				currentPosition--;
			}
			OnDeselected(currentPosition);
			OnSelected(currentPosition);
			letterSlot [currentPosition].GetComponentInChildren<Text> ().text = "";
		}
	}

	public void OpenALetter()
	{
		int letterNumber = 0;
		while (bonusOpen [letterNumber] || letterNumber == 0)
			letterNumber = Random.Range (1, word.Length);
		letterSlot[letterNumber].GetComponentInChildren<Text>().text = word [letterNumber].ToString();
		letterSlot [letterNumber].GetComponent<Button> ().onClick.RemoveAllListeners ();
		bonusOpen [letterNumber] = true;
	}
	
	//Get a number of letters
	public int NumOfLetters(string word)
	{
		int letter = word.Length;
		return letter;
	}
	//Get a fisrt litter from a word
	public char FirstLetter(string word)
	{
		char letter = word[0];
		return letter;
	}

	public void OnSelected(int position){ 
		letterSlot [position].GetComponentInChildren<Text> ().color = Color.yellow;
		letterSlot [position].GetComponent<Image> ().color = Color.yellow;
	}

	public void OnDeselected(int position){
		if (position >= numberOfLetter)
			position = numberOfLetter - 1;
		letterSlot [position].GetComponentInChildren<Text> ().color = Color.white;
		letterSlot [position].GetComponent<Image> ().color = Color.white;
	}
}

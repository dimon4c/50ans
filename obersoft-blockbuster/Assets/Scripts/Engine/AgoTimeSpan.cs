﻿using UnityEngine;
using System.Collections;
using System;

public class AgoTimeSpan : MonoBehaviour {

    private static AgoTimeSpan _instance;

    public static AgoTimeSpan Instance
    {
        get{
            if(_instance == null)
            {
                GameObject go = new GameObject ("AgoTimeSpan");
                go.AddComponent<AgoTimeSpan> ();
                DontDestroyOnLoad(go);
            }
            return _instance;}}

    void Awake()
    {
        _instance = this;
    }

    const int SECOND = 1;
    const int MINUTE = 60 * SECOND;
    const int HOUR = 60 * MINUTE;
    const int DAY = 24 * HOUR;
    const int MONTH = 30 * DAY;

    public string GetRightTimeSpanValue(string dateStr)
    {
        DateTime myDate = DateTime.Parse(dateStr);
        var ts = new TimeSpan(DateTime.UtcNow.Ticks - myDate.Ticks);
        double delta = Math.Abs(ts.TotalSeconds);

        if (delta < 1 * MINUTE)
            return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

        if (delta < 2 * MINUTE)
            return "a minute ago";

        if (delta < 45 * MINUTE)
            return ts.Minutes + " minutes ago";

        if (delta < 90 * MINUTE)
            return "an hour ago";

        if (delta < 24 * HOUR)
            return ts.Hours + " hours ago";

        if (delta < 48 * HOUR)
            return "yesterday";

        if (delta < 10 * DAY)
            return ts.Days + " days ago";
        else
            return "long time ago";
    }
}

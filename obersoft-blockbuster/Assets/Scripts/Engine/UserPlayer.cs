﻿[System.Serializable]
public class UserPlayer {
    public string userName;
    public string realName;
    public string lastPlayed;
    public string photoLink;
    public string bestLetter;
    public string worseLetter;
    public int answerNo;
    public int rightAnswerNo;
    public int score;
    public int weeklyScore;
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
//using SimpleJSON;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.user;
using com.shephertz.app42.paas.sdk.csharp.storage;

using com.shephertz.app42.paas.sdk.csharp.log;

public class MultiplayerUser : MonoBehaviour  {
    Constant cons = new Constant ();

    ServiceAPI sp = null;
    UserService userService = null;  // Initializing User Service.
    UserResponse callBackUser = new UserResponse ();
    StorageService storageService = null; // Initialising Storage Service.
    StorageResponse callBackStorage = new StorageResponse ();

//    public delegate void OnOpponentFound();
//    public event OnOpponentFound onOpponentFound;

	void Awake () 
    {
        sp = new ServiceAPI (cons.apiKey, cons.secretKey);

        //LogService logService = null; // Initializing Log Service.
        //App42Log.SetDebug(true);
	}
	
    void Start ()
    {
        if (!MainGameManager.Instance.HasPlayer)
        {
            string userName = "Player " + UnityEngine.Random.Range(0, 999999).ToString();
            string password = "123456789Pass";
            string userEmail = "email" + UnityEngine.Random.Range(0, 9999).ToString() + "@gmail.com"; 
            Debug.Log("creating user: " + userName);
            CreateUser(userName, password, userEmail);
        } else
        {
            //authenticate after creation done
            Debug.Log("AuthenticateUser: " + MainGameManager.Instance.LocalPlayerId);
            AuthenticateUser(MainGameManager.Instance.LocalPlayerId, MainGameManager.Instance.LocalPlayerPass);
        }

//        PushNotificationsScript.Instance.ShowNotification("PushNotifiaction Message here");

//        App42API.SetLoggedInUser(MainGameManager.Instance.LocalPlayerId);
////        App42Log.SetDebug(true);
//        App42Push.setApp42PushListener (this);
//        #if UNITY_ANDROID
//        string googleProjectNo = "316666677625";
//        App42Push.registerForPush (googleProjectNo);
//        Debug.Log("Last message: " + App42Push.getLastPushMessage()); 
//        #endif 
//        //for iOS??!!
//
//        setListenerGameObject (this.gameObject.name);
    }

    public void CreateUser (string userName, string passwd, string userEmail) 
    {
        userService = sp.BuildUserService (); // Initializing UserService.
        userService.CreateUser(userName, passwd, userEmail, callBackUser);

        /*
        User user = new User();  
        user.SetUserName(userName);  
        User.Profile profile = new User.Profile(user);  
        //profile.SetCountry("USA");  
        //profile.SetCity("Houston");  
        //profile.SetDateOfBirth(new DateTime());  
        profile.SetFirstName(userName);  
        profile.SetLastName("Test");
        //profile.SetHomeLandLine("+1-1800-877-453");  
        //profile.SetOfficeLandLine("+1-1800-111-999");  
        //profile.SetMobile("+958901234571");  
        //profile.SetSex(UserGender.MALE);   
        userService.CreateOrUpdateProfile(user, callBack);  
        */

        //save username and password to playerprefs
        MainGameManager.Instance.LocalPlayerId = userName;
        MainGameManager.Instance.LocalPlayerName = userName;
        MainGameManager.Instance.LocalPlayerPass = passwd;

        CreateUserDoc(false, null);
	}

    public void AuthenticateUser (string userName, string passwd)
    {
        userService = sp.BuildUserService (); // Initializing UserService.
        userService.Authenticate (userName, passwd, callBackUser);
    }

    public void CreateUserDoc (bool update, string oldUserName)
    {
        UserPlayer userJson = new UserPlayer();
        userJson.userName = MainGameManager.Instance.LocalPlayerId;
        userJson.realName = MainGameManager.Instance.LocalPlayerName;
        userJson.lastPlayed = com.shephertz.app42.paas.sdk.csharp.util.Util.GetUTCFormattedTimestamp(System.DateTime.UtcNow);
        userJson.photoLink = MainGameManager.Instance.LocalPlayerPhoto;
        userJson.bestLetter = MainGameManager.Instance.BestLetter;
        userJson.worseLetter = MainGameManager.Instance.WorseLetter;
        userJson.answerNo = MainGameManager.Instance.CorrectAnswers + MainGameManager.Instance.WrongAnswers;
        userJson.rightAnswerNo = MainGameManager.Instance.CorrectAnswers;
        userJson.score = MainGameManager.Instance.TotalScore;
        userJson.weeklyScore = MainGameManager.Instance.WeeklyScore;

        if (oldUserName != null)
            StartCoroutine(FindFbDoc(userJson, oldUserName));
        else
        {
            oldUserName = MainGameManager.Instance.LocalPlayerId;
            storageService = sp.BuildStorageService(); // Initializing Storage Service.
            if (update)
                storageService.UpdateDocumentByKeyValue(cons.dbName, cons.playersCollectionName, "userName", oldUserName, JsonUtility.ToJson(userJson), callBackStorage);
            else
                storageService.InsertJSONDocument(cons.dbName, cons.playersCollectionName, JsonUtility.ToJson(userJson), callBackStorage);
        }
    }

    IEnumerator FindFbDoc (UserPlayer userJson, string oldId)
    {
        //checking if user already has user doc associated with his fb id and getting information from it
        callBackStorage.Reset();
        storageService = sp.BuildStorageService(); // Initializing Storage Service.
        storageService.FindDocumentByKeyValue(cons.dbName, cons.playersCollectionName, "userName", oldId, callBackStorage);
        yield return new WaitUntil (() => callBackStorage.successed || callBackStorage.exceptioned);
        if (callBackStorage.successed)
        {
            string oppStr = callBackStorage.returnedObjects[0];
            UserPlayer oldFbPlayerJson = JsonUtility.FromJson<UserPlayer>(oppStr);
            Debug.Log("old player fb json found: " + oldFbPlayerJson.userName);
            MainGameManager.Instance.BestLetter = oldFbPlayerJson.bestLetter;
            MainGameManager.Instance.WorseLetter = oldFbPlayerJson.worseLetter;
            MainGameManager.Instance.CorrectAnswers = oldFbPlayerJson.rightAnswerNo;
            MainGameManager.Instance.WrongAnswers = oldFbPlayerJson.answerNo - oldFbPlayerJson.rightAnswerNo;
            MainGameManager.Instance.TotalScore = oldFbPlayerJson.score;
            MainGameManager.Instance.WeeklyScore = oldFbPlayerJson.weeklyScore;
            //TODO: update stats UI
            //TODO: set local user level
            //removing guest document
            storageService.DeleteDocumentsByKeyValue(cons.dbName, cons.playersCollectionName, "userName", oldId, callBackStorage);
        } else
        {
            //old doc not found - updating guest doc
            storageService.UpdateDocumentByKeyValue(cons.dbName, cons.playersCollectionName, "userName", oldId, JsonUtility.ToJson(userJson), callBackStorage);
        }
    }

    public void FindOppPlayer ()
    {
        StartCoroutine(FindOpponent());
    }

    IEnumerator FindOpponent ()
    {
        callBackStorage.Reset();

        storageService = sp.BuildStorageService (); // Initializing Storage Service.
        System.DateTime twoDaysAgoDate = System.DateTime.UtcNow;
        // tSpan is 2 days, 0 hours, 00 minutes and 0 second.
        System.TimeSpan tSpan = new System.TimeSpan(2, 0, 0, 0); 
        twoDaysAgoDate.Subtract(tSpan);
        string utcDateFormat = com.shephertz.app42.paas.sdk.csharp.util.Util.GetUTCFormattedTimestamp(twoDaysAgoDate);  
        // building query.  
        Query queryDate = QueryBuilder.Build("lastPlayed", utcDateFormat, Operator.LESS_THAN_EQUALTO);  
        Query queryName = QueryBuilder.Build("userName",  MainGameManager.Instance.LocalPlayerId, Operator.NOT_EQUALS);
        Query opponentQuery = QueryBuilder.CompoundOperator(queryDate, Operator.AND, queryName);   
        storageService.FindDocumentsByQuery(cons.dbName, cons.playersCollectionName,  opponentQuery, callBackStorage);

        yield return new WaitUntil (() => callBackStorage.successed || callBackStorage.exceptioned);

        if (callBackStorage.successed)
        {
            string oppStr = callBackStorage.returnedObjects[UnityEngine.Random.Range(0, callBackStorage.returnedObjects.Count)];
            UserPlayer opponentJson = JsonUtility.FromJson<UserPlayer>(oppStr);
            Debug.Log("opponent found: " + opponentJson.userName);
            UploadGame(opponentJson, opponentJson.userName);
        }
    }

    public void RemoveFinishedGame (Game finishedGame)
    {
        if (finishedGame.redPlayerId == MainGameManager.Instance.LocalPlayerId)
            finishedGame.redPlayerId = " ";
        else
            finishedGame.bluePlayerId = " ";
        storageService = sp.BuildStorageService (); // Initializing Storage Service.
        storageService.UpdateDocumentByKeyValue(cons.dbName, cons.gamesCollectionName, "gameId", finishedGame.gameId, GameToJson(finishedGame), callBackStorage);
    }

    public void UploadGame (UserPlayer opponentId, string userForPush)
    {
        CreateUserDoc(true, null);
        if (opponentId != null)
        {
            SessionHandler.Instance.currentSession.bluePlayerId = opponentId.userName;
            SessionHandler.Instance.currentSession.bluePlayerName = opponentId.realName;
            SessionHandler.Instance.currentSession.bluePlayerPhotoLink = opponentId.photoLink;
        }
        StartCoroutine(UploadingProcess(opponentId == null, userForPush));
    }

    IEnumerator UploadingProcess (bool update, string userForPush)
    {
        callBackStorage.Reset();

        storageService = sp.BuildStorageService(); // Initializing Storage Service.
        if (update)
        {
//            string playerColor = "redPlayerId";
//            if (MainGameManager.Instance.LocalPlayerId == SessionHandler.Instance.currentSession.bluePlayerId)
//                playerColor = "bluePlayerId";
//            Query queryId = QueryBuilder.Build("gameId", SessionHandler.Instance.currentSession.gameId, Operator.EQUALS);  
//            Query playerId = QueryBuilder.Build(playerColor, MainGameManager.Instance.LocalPlayerId, Operator.EQUALS);
//            Query playerGamesQuery = QueryBuilder.CompoundOperator(queryId, Operator.AND, playerId);
//            Debug.Log(playerGamesQuery.GetStr());
//            storageService.UpdateDocumentByQuery(cons.dbName, cons.gamesCollectionName, playerGamesQuery, GameToJson(SessionHandler.Instance.currentSession), callBackStorage);

            storageService.UpdateDocumentByKeyValue(cons.dbName, cons.gamesCollectionName, "gameId", SessionHandler.Instance.currentSession.gameId, GameToJson(SessionHandler.Instance.currentSession), callBackStorage);
        } else
        {
            storageService.InsertJSONDocument(cons.dbName, cons.gamesCollectionName, GameToJson(SessionHandler.Instance.currentSession), callBackStorage);
        }
        yield return new WaitUntil (() => callBackStorage.successed || callBackStorage.exceptioned);

        if (callBackStorage.successed)
        {
            string message = MainGameManager.Instance.LocalPlayerName;
            if (update)
                message += " made his turn!";
            else
                message += " invited you to play!";
            PushNotificationsScript.Instance.SendPushMessageToUser(userForPush, message);

            //update game list
            MultiplayerGames gamesLoadScript = Camera.main.GetComponent<MultiplayerGames>();
            gamesLoadScript.GetPlayerGames();
        }
    }

    public void LoadWeeklyLeaderboard ()
    {
        //TODO: get last Sunday/Monday 12.00PM
        System.DateTime weekAgoDate = System.DateTime.UtcNow;
        // tSpan is 2 days, 0 hours, 00 minutes and 0 second.
        System.TimeSpan tSpan = new System.TimeSpan(7, 0, 0, 0); 
        weekAgoDate.Subtract(tSpan);
        string utcDateFormat = com.shephertz.app42.paas.sdk.csharp.util.Util.GetUTCFormattedTimestamp(weekAgoDate);  
        // building query.  
        Query queryDate = QueryBuilder.Build("lastPlayed", utcDateFormat, Operator.LESS_THAN_EQUALTO);  
        Query queryName = QueryBuilder.Build("weeklyScore",  0, Operator.GREATER_THAN);
        Query opponentQuery = QueryBuilder.CompoundOperator(queryDate, Operator.AND, queryName);

        storageService = sp.BuildStorageService (); // Initializing Storage Service.   
        storageService.FindDocumentsByQuery(cons.dbName, cons.playersCollectionName,  opponentQuery, callBackStorage);
        //TODO: check loading and representing players with fb links if they have
    }

    string GameToJson (Game gameToUpdate)
    {
        string newGameString = JsonUtility.ToJson(gameToUpdate);
        return newGameString;
    }
}

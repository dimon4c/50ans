using System;
using com.shephertz.app42.paas.sdk.csharp.storage;
using com.shephertz.app42.paas.sdk.csharp;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class StorageResponse : App42CallBack
{
    public bool successed = false;
    public bool exceptioned = false;
    private string result = "";
    public List<string> returnedObjects;

    public void OnSuccess(object obj)
    {
        successed = true;
        result = obj.ToString();
        if (obj is Storage)
        {
            Storage storage = (Storage)obj;
            Debug.Log ("Storage Response : " + storage);
            IList<Storage.JSONDocument> jsonDocList = storage.GetJsonDocList();
            returnedObjects = new List<string>();
            for(int i=0;i<storage.GetJsonDocList().Count;i++){
//                Debug.Log ("DocId is : " + jsonDocList[i].GetDocId());
//                Debug.Log ("jsonDoc is : " + jsonDocList[i].GetJsonDoc());
                returnedObjects.Add(jsonDocList[i].GetJsonDoc());
            }
        }
    }


    public void OnException(Exception e)
    {
        exceptioned = true;
        result = e.ToString();
        Debug.Log ("Exception is : " + e);

    }

    public string getResult() 
    {
        return result;
    }

    public void Reset ()
    {
        successed = false;
        exceptioned = false;
    }
}


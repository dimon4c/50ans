﻿using System;
using com.shephertz.app42.paas.sdk.csharp.storage;
using com.shephertz.app42.paas.sdk.csharp;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GamesResponse : App42CallBack
{
    public bool successed = false;
    public bool exceptioned = false;
    public string result = "";
    public List<string> gamesList;

    public void OnSuccess(object obj)
    {
        successed = true;
        result = obj.ToString();
        if (obj is Storage)
        {
            Storage storage = (Storage)obj;
//            Debug.Log ("Storage Response : " + storage);
            IList<Storage.JSONDocument> jsonDocList = storage.GetJsonDocList();
            gamesList = new List<string>();
            for(int i=0;i<storage.GetJsonDocList().Count;i++){
//                Debug.Log ("DocId is : " + jsonDocList[i].GetDocId());
//                Debug.Log ("jsonDoc is : " + jsonDocList[i].GetJsonDoc());
                gamesList.Add(jsonDocList[i].GetJsonDoc());
            }
//            Debug.Log("game list [0]: " + gamesList[0]);
            //update games on home screen
            HomeScreen homeScreenScript = Camera.main.GetComponent<HomeScreen>();
            if (homeScreenScript)
                homeScreenScript.LoadGamesFromStorage(gamesList);
        }
    }


    public void OnException(Exception e)
    {
        exceptioned = true;
        result = e.ToString();
        Debug.Log ("Exception is : " + e);

    }

    public string getResult() 
    {
        return result;
    }

    public void Reset ()
    {
        successed = false;
        exceptioned = false;
    }
}

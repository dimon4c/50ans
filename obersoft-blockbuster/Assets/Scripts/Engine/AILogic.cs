﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AILogic : MonoBehaviour {
	public HomeScreen homeScreenScript;
	List<List<int[]>> allPaths;

	bool HasBlueNeighboursUp (int xCoord, int yCoord, Game oppTurnGame)
	{
		if (yCoord == 0)
			return true;
		List <int[]> neighboursOfCell = GetUpBlueCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
		{
			int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
			if (oppTurnGame.cellValues [cellValuesIndex] == 2)
				return true;
		}
		return false;
	}

	List<int[]> GetUpBlueCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (yCoord > 0) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord - 1});
			if (xCoord%2 == 0) 
			{
				if (xCoord > 0)
					neighbours.Add (new int[]{ xCoord - 1, yCoord - 1});
				if (xCoord < 4)
					neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
			}
		}
		if (xCoord%2 == 1)
		{
			if (xCoord > 0)
				neighbours.Add (new int[]{ xCoord - 1, yCoord});
			if (xCoord < 4)
				neighbours.Add (new int[]{ xCoord + 1, yCoord});
		}
		return neighbours;
	}

	bool HasBlueNeighboursDown (int xCoord, int yCoord, Game oppTurnGame)
	{
		if (yCoord == 3)
			return true;
		List <int[]> neighboursOfCell = GetDownBlueCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
		{
			int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
			if (oppTurnGame.cellValues [cellValuesIndex] == 2)
				return true;
		}
		return false;
	}

	List<int[]> GetDownBlueCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (yCoord < 3) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord + 1});
			if (xCoord%2 == 1) 
			{
				if (xCoord > 0)
					neighbours.Add (new int[]{ xCoord - 1, yCoord + 1});
				if (xCoord < 4)
					neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
			}
		}
		if (xCoord%2 == 0)
		{
			if (xCoord > 0)
				neighbours.Add (new int[]{ xCoord - 1, yCoord});
			if (xCoord < 4)
				neighbours.Add (new int[]{ xCoord + 1, yCoord});
		}
		return neighbours;
	}

	bool HasRedNeighboursLeft (int xCoord, int yCoord, Game oppTurnGame)
	{
		if (xCoord == 0)
			return true;
		List <int[]> neighboursOfCell = GetLeftRedCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
		{
			int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
			if (oppTurnGame.cellValues [cellValuesIndex] == 1)
				return true;
		}
		return false;
	}

	List<int[]> GetLeftRedCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (xCoord > 0) 
		{
			neighbours.Add (new int[]{ xCoord - 1, yCoord});
			if (yCoord > 0) 
			{
				if (xCoord%2 == 0)
					neighbours.Add (new int[]{ xCoord - 1, yCoord - 1});
			}
			if (yCoord < 3) 
			{
				if (xCoord%2 == 1)
					neighbours.Add (new int[]{ xCoord - 1, yCoord + 1});
			}
		}
		return neighbours;
	}

	bool HasRedNeighboursRight (int xCoord, int yCoord, Game oppTurnGame)
	{
		if (xCoord == 4)
			return true;
		List <int[]> neighboursOfCell = GetRightRedCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
		{
			int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
			if (oppTurnGame.cellValues [cellValuesIndex] == 1)
				return true;
		}
		return false;
	}

	public List<int[]> GetRightRedCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (xCoord < 4) 
		{
			neighbours.Add (new int[]{ xCoord + 1, yCoord});
			if (yCoord > 0) 
			{
				if (xCoord%2 == 0)
					neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
			}
			if (yCoord < 3) 
			{
				if (xCoord%2 == 1)
					neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
			}
		}
		return neighbours;
	}

	public void AITurns ()
	{
		for (int gameIndex = 0; gameIndex < homeScreenScript.opponentTurnGames.Count; gameIndex++) 
		{
            Game editedOppTurnGame = homeScreenScript.opponentTurnGames [gameIndex];
            System.DateTime dateOfLastTurn = System.DateTime.Parse(editedOppTurnGame.lastTurn);
            System.TimeSpan differenceInTime = System.DateTime.UtcNow - dateOfLastTurn;
            if (differenceInTime.Days >= 2) //fake opponents turn if last turn is 2 days old
            {
                homeScreenScript.opponentTurnGames [gameIndex] = MakeAITurn (editedOppTurnGame);
                Debug.Log("AI MADE IT'S TURN!");
                SessionHandler.Instance.currentSession = homeScreenScript.opponentTurnGames[gameIndex];
                homeScreenScript.userService.UploadGame(null, MainGameManager.Instance.LocalPlayerId);
                //upload game to server
                //notify local user
            }
//			Debug.Log ("game ID: " + editedOppTurnGame.gameId);
//			MainGameManager.Instance.SaveGames (MainGameManager.Instance.LoadGames(), editedOppTurnGame);
		}
//		homeScreenScript.LoadGames ();
	}

	Game MakeAITurn (Game oppTurnGame) 
	{
        oppTurnGame.lastTurn = System.DateTime.UtcNow.ToString();

		if (oppTurnGame.isThereAnswerCell) 
		{
			Debug.Log ("AnswertoCell");
			if (AIAnswersCorrect ()) 
			{
				Debug.Log ("AI answers correct");
				//change cell value and set cell to answer to false
				oppTurnGame.isThereAnswerCell = false;
				int cellToAnswerIndex = oppTurnGame.cellToAnswerY * 5;
				cellToAnswerIndex += oppTurnGame.cellToAnswerX;
                if(System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId)) //AI red
//				if (!oppTurnGame.localPlayerRed)
					oppTurnGame.cellValues [cellToAnswerIndex] = 1;
				else
					oppTurnGame.cellValues [cellToAnswerIndex] = 2;

				//check for win
                if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId) && IsThereWinningPathForRed (oppTurnGame.cellValues))
				{
					oppTurnGame.finished = true;
                    oppTurnGame.redWins = true;
				} else
					oppTurnGame = MakeAITurn (oppTurnGame);
			} else 
			{
				Debug.Log ("AI failed to answer");
				oppTurnGame.isThereAnswerCell = false;
				int cellToAnswerIndex = oppTurnGame.cellToAnswerY * 5;
				cellToAnswerIndex += oppTurnGame.cellToAnswerX;
				oppTurnGame.cellValues [cellToAnswerIndex] = 3;
                if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                    oppTurnGame.turn = "blue";
                else
                    oppTurnGame.turn = "red";
				//end turn and so on
				return oppTurnGame;
			}
		} else 
		{
            if(System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
			{
				int cellAIAnswerX = 0;
				int cellAIAnswerY = 0;
				bool cellAIAnswerFound = false;
				for (int xCoord = 0; xCoord < 5; xCoord++) 
				{
					for (int yCoord = 0; yCoord < 3; yCoord++) 
					{
						if (!cellAIAnswerFound)
						{
							int cellInd = yCoord * 5 + xCoord;
							if (oppTurnGame.cellValues [cellInd] == 1) 
							{
								//check if has left and right neighbours with the same color and not staying on the border
								//if hasnt't 1 of them check if can add 1
								//check for left or right neighbours to answer
								bool hasLeftRedNeighbours = HasRedNeighboursLeft (xCoord, yCoord, oppTurnGame);
								bool hasRightRedNeighbours = HasRedNeighboursRight (xCoord, yCoord, oppTurnGame);
								List <int[]> canBeUsedNeighbours = new List<int[]> ();
								if (hasLeftRedNeighbours && !hasRightRedNeighbours) 
								{
									//check to add right neighbour
									List <int[]> neighboursOfCell = GetRightRedCellNeighbours (xCoord, yCoord);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] 
												{
													neighboursOfCell [neighbourInd] [0] ,
													neighboursOfCell [neighbourInd] [1]
												});
									}
								} else if (!hasLeftRedNeighbours && hasRightRedNeighbours) 
								{
									//check to add left neighbour
									List <int[]> neighboursOfCell = GetLeftRedCellNeighbours (xCoord, yCoord);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] {
												neighboursOfCell [neighbourInd] [0] ,
												neighboursOfCell [neighbourInd] [1]
											});
									}
								} else 
								{
									//add a neighbour
									List <int[]> neighboursOfCell = GetRightRedCellNeighbours (xCoord, yCoord);
									List <int[]> neighboursOfCellL = GetLeftRedCellNeighbours (xCoord, yCoord);
									neighboursOfCell.AddRange (neighboursOfCellL);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] {
												neighboursOfCell [neighbourInd] [0] ,
												neighboursOfCell [neighbourInd] [1]
											});
									}
								}
								if (canBeUsedNeighbours.Count > 0) 
								{
									cellAIAnswerFound = true;
									int randomNeighbourIndex = Random.Range (0, canBeUsedNeighbours.Count);
									cellAIAnswerX = canBeUsedNeighbours [randomNeighbourIndex] [0];
									cellAIAnswerY = canBeUsedNeighbours [randomNeighbourIndex] [1];
								}
							}
						}
					}
				}
				if (cellAIAnswerFound)
				{
					Debug.Log ("found cell to make neighbour");
					if (AIAnswersCorrect())
					{
						Debug.Log ("AI answers correct");
						int cellToAnswerIndex = cellAIAnswerY * 5;
						cellToAnswerIndex += cellAIAnswerX;
						oppTurnGame.cellValues [cellToAnswerIndex] = 1;

						if (IsThereWinningPathForRed (oppTurnGame.cellValues))
						{
							oppTurnGame.finished = true;
                            oppTurnGame.redWins = true;
						} else
							oppTurnGame = MakeAITurn (oppTurnGame);
					} else
					{
						Debug.Log ("AI failed to answer");
						int cellToAnswerIndex = cellAIAnswerY * 5;
						cellToAnswerIndex += cellAIAnswerX;
						oppTurnGame.isThereAnswerCell = true;
						oppTurnGame.cellToAnswerX = cellAIAnswerX;
						oppTurnGame.cellToAnswerY = cellAIAnswerY;
                        if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                            oppTurnGame.turn = "blue";
                        else
                            oppTurnGame.turn = "red";
						//end turn and so on
						return oppTurnGame;
					}
				} else
				{
					//no neighbours so find first empty cell
					bool emptyCellFound = false;
					for (int xCoord = 0; xCoord < 5; xCoord++) 
					{
						for (int yCoord = 0; yCoord < 3; yCoord++) 
						{
							if (!emptyCellFound) 
							{
								int cellInd = yCoord * 5 + xCoord;
								if (oppTurnGame.cellValues [cellInd] == 0) 
								{
									emptyCellFound = true;
									Debug.Log ("found  empty cell to answer");
									if (AIAnswersCorrect())
									{
										Debug.Log ("AI answers correct");
										oppTurnGame.cellValues [cellInd] = 1;

										if (IsThereWinningPathForRed (oppTurnGame.cellValues))
										{
											oppTurnGame.finished = true;
                                            oppTurnGame.redWins = true;
										} else
											oppTurnGame = MakeAITurn (oppTurnGame);
									} else
									{
										Debug.Log ("AI failed to answer");
										oppTurnGame.isThereAnswerCell = true;
										oppTurnGame.cellToAnswerX = cellAIAnswerX;
										oppTurnGame.cellToAnswerY = cellAIAnswerY;
                                        if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                                            oppTurnGame.turn = "blue";
                                        else
                                            oppTurnGame.turn = "red";
										//end turn and so on
										return oppTurnGame;
									}
								}
							}
						}
					}
					if (!emptyCellFound)
					{
						//finish game AI loses
						Debug.Log ("not found option. AI loses");
						oppTurnGame.finished = true;
                        if (System.String.Equals(oppTurnGame.redPlayerId, MainGameManager.Instance.LocalPlayerId))
                            oppTurnGame.redWins = true;
					}
				}
			} else // blue AI turn
			{
				int cellAIAnswerX = 0;
				int cellAIAnswerY = 0;
				bool cellAIAnswerFound = false;
				for (int yCoord = 0; yCoord < 3; yCoord++) 
				{
					for (int xCoord = 0; xCoord < 5; xCoord++)
					{
						if (!cellAIAnswerFound)
						{
							int cellInd = yCoord * 5 + xCoord;
							if (oppTurnGame.cellValues [cellInd] == 2) 
							{
								//check if has left and right neighbours with the same color and not staying on the border
								//if hasnt't 1 of them check if can add 1
								//check for left or right neighbours to answer
								bool hasUpBlueNeighbours = HasBlueNeighboursUp (xCoord, yCoord, oppTurnGame);
								bool hasDownBlueNeighbours = HasBlueNeighboursDown (xCoord, yCoord, oppTurnGame);
								List <int[]> canBeUsedNeighbours = new List<int[]> ();
								if (hasUpBlueNeighbours && !hasDownBlueNeighbours) 
								{
									//check to add down neighbour
									List <int[]> neighboursOfCell = GetDownBlueCellNeighbours (xCoord, yCoord);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] 
												{
													neighboursOfCell [neighbourInd] [0] ,
													neighboursOfCell [neighbourInd] [1]
												});
									}
								} else if (!hasUpBlueNeighbours && hasDownBlueNeighbours) 
								{
									//check to add up neighbour
									List <int[]> neighboursOfCell = GetUpBlueCellNeighbours (xCoord, yCoord);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] {
												neighboursOfCell [neighbourInd] [0] ,
												neighboursOfCell [neighbourInd] [1]
											});
									}
								} else 
								{
									//add a neighbour
									List <int[]> neighboursOfCell = GetUpBlueCellNeighbours (xCoord, yCoord);
									List <int[]> neighboursOfCellD = GetDownBlueCellNeighbours (xCoord, yCoord);
									neighboursOfCell.AddRange (neighboursOfCellD);
									for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
									{
										int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
										if (oppTurnGame.cellValues [cellValuesIndex] == 0)
											canBeUsedNeighbours.Add (new int[] {
												neighboursOfCell [neighbourInd] [0] ,
												neighboursOfCell [neighbourInd] [1]
											});
									}
								}
								if (canBeUsedNeighbours.Count > 0) 
								{
									cellAIAnswerFound = true;
									int randomNeighbourIndex = Random.Range (0, canBeUsedNeighbours.Count);
									cellAIAnswerX = canBeUsedNeighbours [randomNeighbourIndex] [0];
									cellAIAnswerY = canBeUsedNeighbours [randomNeighbourIndex] [1];
								}
							}
						}
					}
				}
				if (cellAIAnswerFound)
				{
					if (AIAnswersCorrect())
					{
						Debug.Log ("BLUE AI: answers correct");
						int cellToAnswerIndex = cellAIAnswerY * 5;
						cellToAnswerIndex += cellAIAnswerX;
						oppTurnGame.cellValues [cellToAnswerIndex] = 2;

						if (IsThereWinningPathForRed (oppTurnGame.cellValues))
						{
							oppTurnGame.finished = true;
                            oppTurnGame.redWins = false;
						} else
							oppTurnGame = MakeAITurn (oppTurnGame);
					} else
					{
						Debug.Log ("BLUE AI: failed to answer");
						int cellToAnswerIndex = cellAIAnswerY * 5;
						cellToAnswerIndex += cellAIAnswerX;
						oppTurnGame.isThereAnswerCell = true;
						oppTurnGame.cellToAnswerX = cellAIAnswerX;
						oppTurnGame.cellToAnswerY = cellAIAnswerY;
                        if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                            oppTurnGame.turn = "blue";
                        else
                            oppTurnGame.turn = "red";
						//end turn and so on
						return oppTurnGame;
					}
				} else
				{
					//no neighbours so find first empty cell
					bool emptyCellFound = false;
					for (int yCoord = 0; yCoord < 3; yCoord++) 
					{
						for (int xCoord = 0; xCoord < 5; xCoord++)
						{
							if (!emptyCellFound) 
							{
								int cellInd = yCoord * 5 + xCoord;
								if (oppTurnGame.cellValues [cellInd] == 0) 
								{
									emptyCellFound = true;
									Debug.Log ("BLUE AI: found  empty cell to answer");
									if (AIAnswersCorrect())
									{
										Debug.Log ("BLUE AI: answers correct");
										oppTurnGame.cellValues [cellInd] = 2;

										if (IsThereWinningPathForRed (oppTurnGame.cellValues))
										{
											oppTurnGame.finished = true;
                                            oppTurnGame.redWins = false;
										} else
											oppTurnGame = MakeAITurn (oppTurnGame);
									} else
									{
										Debug.Log ("BLUE AI: failed to answer");
										oppTurnGame.isThereAnswerCell = true;
										oppTurnGame.cellToAnswerX = cellAIAnswerX;
										oppTurnGame.cellToAnswerY = cellAIAnswerY;
                                        if (System.String.Equals (oppTurnGame.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                                            oppTurnGame.turn = "blue";
                                        else
                                            oppTurnGame.turn = "red";
										//end turn and so on
										return oppTurnGame;
									}
								}
							}
						}
					}
					if (!emptyCellFound)
					{
						//finish game AI loses
						Debug.Log ("BLUE AI: not found option. AI loses");
						oppTurnGame.finished = true;
                        if (System.String.Equals(oppTurnGame.redPlayerId, MainGameManager.Instance.LocalPlayerId))
                            oppTurnGame.redWins = true;
					}
				}
			}
		}
		return oppTurnGame;
	}


	int GetCorrectAnswerProbability ()
	{
		float allAnswers = MainGameManager.Instance.WrongAnswers + MainGameManager.Instance.CorrectAnswers;
		if (allAnswers > 5) {
			float percentOfRightAnswers = MainGameManager.Instance.CorrectAnswers / allAnswers * 100;
			return (int)percentOfRightAnswers;
		} else
			return 50;
		/*
		int correctAnswerProb = 0;
		switch (MainGameManager.Instance.LocalPlayerLevel)
		{
		case 1:
			correctAnswerProb = 30;
			break;
		case 2:
			correctAnswerProb = 50;
			break;
		case 3:
			correctAnswerProb = 60;
			break;
		case 4:
			correctAnswerProb = 70;
			break;
		}
		return correctAnswerProb;
		*/
	}

	bool AIAnswersCorrect ()
	{
		int answersPercent = Random.Range (0, 100);
		if (answersPercent < GetCorrectAnswerProbability ())
			return true;
		else
			return false;
	}

	#region red player paths
	void CreateRedPathFromCellandPath(int xCoord, int yCoord, List<int[]> curPath, int[] pseudoField)
	{
		if (curPath == null)
		{
			curPath = new List<int[]> ();
		}

		int cellValuesIndexR = xCoord + yCoord * 5;
		if (pseudoField [cellValuesIndexR] != 1)
			return;

		curPath.Add (new int[]{ xCoord, yCoord });
		allPaths.Add (curPath);
		List <int[]> neighboursOfCell = GetRedCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) {
			//if (!curPath.Contains (neighboursOfCell [neighbourInd])) 
			if (curPath.Count > 1) {
				//checks if array path already has cell
				bool alreadyHasTheCell = false;
				for (int coordIndex = 0; coordIndex < curPath.Count; coordIndex++)
				{
					if (curPath [coordIndex] [0] == neighboursOfCell [neighbourInd] [0] && curPath [coordIndex] [1] == neighboursOfCell [neighbourInd] [1])
						alreadyHasTheCell = true;
				}
				if (!alreadyHasTheCell)
				{
					int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
					if (pseudoField [cellValuesIndex] == 1) {
						CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
					}

				}
			} else {
				int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
				if (pseudoField [cellValuesIndex] == 1) {
					CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
				}
			}
		}
	}

	public List<int[]> GetRedCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (yCoord > 0) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord - 1});
			if (xCoord < 4 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
		}
		if (yCoord < 3) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord + 1});
			if (xCoord%2 == 1 && xCoord < 4)
				neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
		}
		if (xCoord < 4)
			neighbours.Add (new int[]{ xCoord + 1, yCoord});
		return neighbours;
	}

	public void CheckRedPaths ()
	{
		bool hasWinningPath = false;
		List<int[]> largestPath = new List<int[]> ();
		foreach (List<int[]> path in allPaths) 
		{
			if (largestPath.Count == 0)
				largestPath = path;
			if (path [path.Count - 1] [0] == 4) 
			{
				hasWinningPath = true;
				largestPath = path;
			} else if (path [path.Count - 1] [0] > largestPath [largestPath.Count - 1] [0]) 
			{
				largestPath = path;
			} else if (path [path.Count - 1] [0] == largestPath [largestPath.Count - 1] [0] &&
				path.Count > largestPath.Count)
			{
				largestPath = path;
			}
		}

		//log largest path
		Debug.Log("Has winning path red:" + hasWinningPath + "\n");
		Debug.Log("Largest path blue:\n");
		for (int i = 0; i < largestPath.Count; i++) 
		{
			Debug.Log (largestPath [i] [0] + " " + largestPath [i] [1] + "\n");
		}
	}

	public bool IsThereWinningPathForRed (int[] pseudoField)
	{
		allPaths = new List<List<int[]>> ();
		CreateRedPathFromCellandPath (0, 0, null, pseudoField);
		CreateRedPathFromCellandPath (0, 1, null, pseudoField);
		CreateRedPathFromCellandPath (0, 2, null, pseudoField);
		CreateRedPathFromCellandPath (0, 3, null, pseudoField);
		bool hasWinningPath = false;
		foreach (List<int[]> path in allPaths) 
		{
			if (path [path.Count - 1] [0] == 4) 
			{
				hasWinningPath = true;
			}
		}
		return hasWinningPath;
	}
	#endregion
}

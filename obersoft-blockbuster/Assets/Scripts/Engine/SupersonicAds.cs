﻿using UnityEngine;
using System.Collections;

public class SupersonicAds : MonoBehaviour
{
    public string androidAppKey = "4d9f3435";
    public string iOSAppKey = "4d9efaad";

    void Start()
    {
        Supersonic.Agent.start();
        //Set the unique id of your end user.
        string uniqueUserId = "APPLICATION_USER_ID_HERE";
        string appKey = "";
        #if UNITY_ANDROID
        appKey = androidAppKey;
        #elif Unity_IOS
        appKey = iOSAppKey;
        #endif
        //Init Rewarded Video
        Supersonic.Agent.initRewardedVideo(appKey, uniqueUserId);
    }

    public void ShowRewardedVideo()
    {
        if (Supersonic.Agent.isRewardedVideoAvailable())
            Supersonic.Agent.showRewardedVideo();
    }

    void OnEnable()
    {
        SupersonicEvents.onRewardedVideoInitSuccessEvent += RewardedVideoInitSuccessEvent;

        SupersonicEvents.onRewardedVideoInitFailEvent += RewardedVideoInitFailEvent;

        SupersonicEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;

        SupersonicEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;

        SupersonicEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;

        SupersonicEvents.onVideoAvailabilityChangedEvent += VideoAvailabilityChangedEvent;

        SupersonicEvents.onVideoStartEvent += VideoStartEvent;

        SupersonicEvents.onVideoEndEvent += VideoEndEvent;
    }

    void OnDisable()
    {
        SupersonicEvents.onRewardedVideoInitSuccessEvent -= RewardedVideoInitSuccessEvent;

        SupersonicEvents.onRewardedVideoInitFailEvent -= RewardedVideoInitFailEvent;

        SupersonicEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;

        SupersonicEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;

        SupersonicEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;

        SupersonicEvents.onVideoAvailabilityChangedEvent -= VideoAvailabilityChangedEvent;

        SupersonicEvents.onVideoStartEvent -= VideoStartEvent;

        SupersonicEvents.onVideoEndEvent -= VideoEndEvent;
    }

    //Invoked when initialization of RewardedVideo has finished successfully.
    void RewardedVideoInitSuccessEvent()
    {
    }
    //Invoked when RewardedVideo initialization process has failed.
    //SupersonicError contains the reason for the failure.
    void RewardedVideoInitFailEvent(SupersonicError error)
    {

        Debug.Log("Init rewarded video error ");

    }
    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
    }
    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available.
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    void VideoAvailabilityChangedEvent(bool available)
    {
        //Change the in-app 'Traffic Driver' state according to availability.
        bool rewardedVideoAvailability = available;
    }
    //Invoked when the video ad starts playing.
    void VideoStartEvent()
    {
    }
    //Invoked when the video ad finishes playing.
    void VideoEndEvent()
    {
    }
    //Invoked when the user completed the video and should be rewarded.
    //If using server-to-server callbacks you may ignore this events and wait for
    //the callback from the Supersonic server.
    //@param - placement - placement object which contains the reward data
    void RewardedVideoAdRewardedEvent(SupersonicPlacement ssp)
    {
        //TODO - here you can reward the user according to the reward name and amount
        MainGameManager.Instance.PlayerCoins += 3;
//        ssp.getPlacementName();
//        ssp.getRewardName());
//        ssp.getRewardAmount();
    }
}

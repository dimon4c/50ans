﻿using UnityEngine;
using UnityEngine.UI;

namespace UnitySampleAssets.Utility
{
    public class FPSCounter : MonoBehaviour
    {
        private float fpsMeasurePeriod = 0.5f;
        private int fpsAccumulator = 0;
        private float fpsNextPeriod = 0;
        private int currentFps;
        private string display = "{0} FPS";
        public Text fpsText;

        private void Start()
        {
            fpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        }

        private void Update()
        {
            // measure average frames per second
            fpsAccumulator++;
            if (Time.realtimeSinceStartup > fpsNextPeriod)
            {
                currentFps = (int) (fpsAccumulator/fpsMeasurePeriod);
                fpsAccumulator = 0;
                fpsNextPeriod += fpsMeasurePeriod;
                fpsText.text = string.Format(display, currentFps);
                if (currentFps > 29.9f)
                    fpsText.color = Color.green;
                else if (currentFps > 20)
                    fpsText.color = Color.yellow;
                else if (currentFps > 0)
                    fpsText.color = Color.red;
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections;

public class SessionHandler : MonoBehaviour {

	public Game currentSession;
	public Cell currentCell;

	// Static singleton instance
	private static SessionHandler instance;

	void Awake () {
		if(instance == null) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else Destroy(this); // or gameObject
	}

	// Static singleton property
	public static SessionHandler Instance
	{
		// Here we use the ?? operator, to return 'instance' if 'instance' does not equal null
		// otherwise we assign instance to a new component and return that
		get { return instance ?? (instance = new GameObject("SessionHandler").AddComponent<SessionHandler>()); }
	}

	// Instance method, this method can be accesed through the singleton instance
	public void DoSomeAwesomeStuff()
	{
		Debug.Log("I'm doing awesome stuff");
	}
}

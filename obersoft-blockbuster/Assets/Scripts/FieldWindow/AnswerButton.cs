﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {
	public Image buttonSprite;
	public Sprite activeSprite;
    public Sprite nonActiveSprite;
    public Sprite wrongSprite;
    public Sprite correctSprite;
	public Text answerText;
	public Button btnScript;
    public Image flagImage;
    public bool hasFlag = false;

	Color activeTextColor = new Color (47 / 255.0f, 47 / 255.0f, 47 / 255.0f);
    Color nonActiveTextColor = new Color (172 / 255.0f, 172 / 255.0f, 172 / 255.0f);
    Color wrongTextColor = new Color (255 / 255.0f, 223 / 255.0f, 223 / 255.0f);
    Color correctTextColor = new Color (43 / 255.0f, 81 / 255.0f, 9 / 255.0f);


	public void SetAnswer (string answer)
	{
		btnScript.enabled = true;
		answerText.text = answer;
		buttonSprite.sprite = activeSprite;
		answerText.color = activeTextColor;
	}

	public void SetNonActive ()
	{
		btnScript.enabled = false;
		buttonSprite.sprite = nonActiveSprite;
        answerText.color = nonActiveTextColor;
	}

    public void SetWrong ()
    {
        btnScript.enabled = false;
        buttonSprite.sprite = wrongSprite;
        answerText.color = wrongTextColor;
    }

    public void SetCorrect ()
    {
        btnScript.enabled = false;
        buttonSprite.sprite = correctSprite;
        answerText.color = correctTextColor;
    }

    public void ShowFlag () 
    {
        if (hasFlag)
            flagImage.gameObject.SetActive(true);
    }

    public void HideFlag ()
    {
        if (hasFlag)
            flagImage.gameObject.SetActive(false);
    }
}

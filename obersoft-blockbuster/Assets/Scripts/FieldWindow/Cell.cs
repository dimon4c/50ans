﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cell : MonoBehaviour, IPointerClickHandler
{

    /*
    public int cellX;
    public int cellY;
    */
    public int cellType = 0;
    //0 -just cell. 1 -red player cell. 2 -blue player. cell 3 - black cell.
    public string cellValue;

    public Text cellTitle;

    /*
    public GameObject blueImage;
    public GameObject redImage;
    public GameObject blackImage;
    public GameObject highlightedImage;
    public GameObject categoryImage;

    public Sprite[] redHighlighted;
    public Sprite[] blueHighlighted;
    public Sprite[] blackHighlighted;

    public GameObject challengeConfirmation;
    */
    public void SetCellValue(string newValue, bool image, Sprite categorySprite)
    {
        cellValue = newValue;
        if (image)
        {
            /*
            categoryImage.GetComponent<Image>().sprite = categorySprite;
            categoryImage.SetActive(true);
            */
        }
        else
        {
            cellTitle.text = newValue;
            cellTitle.gameObject.SetActive(true);
        }
    }
    /*
    public void HighlightCell()
    {
        switch (cellType)
        {
            case 1:
                highlightedImage.GetComponent<Image>().sprite = redHighlighted[Random.Range(0, redHighlighted.Length)];
                break;
            case 2:
                highlightedImage.GetComponent<Image>().sprite = blueHighlighted[Random.Range(0, blueHighlighted.Length)];
                break;
            case 3:
                highlightedImage.GetComponent<Image>().sprite = blackHighlighted[Random.Range(0, blackHighlighted.Length)];
                break;
        }
        highlightedImage.SetActive(true);
    }

    public void DehighlightCell()
    {
        highlightedImage.SetActive(false);
    }

    public void SetCellType(int newType)
    {
        cellType = newType;
        redImage.SetActive(false);
        blueImage.SetActive(false);
        blackImage.SetActive(false);
        switch (newType)
        {
            case 1:
                redImage.SetActive(true);
                if (cellTitle)
                    cellTitle.color = Color.white;
                break;
            case 2:
                blueImage.SetActive(true);
                if (cellTitle)
                    cellTitle.color = Color.white;
                break;
            case 3:
                blackImage.SetActive(true);
                if (cellTitle)
                    cellTitle.color = Color.white;
                break;
            default:
                if (cellTitle)
                    cellTitle.color = new Color(50 / 255.0f, 50 / 255.0f, 50 / 255.0f);
                break;
        }
    }
    */

    void CellTapped()
    {
        GamePlay playScript = FindObjectOfType<GamePlay>();
        if (playScript.loadingQuestion)
            return;
		
        Debug.Log(cellValue + " choosen");
        switch (cellType)
        {
            case 0:
			// show question screen
                if (!playScript.challengeMode)
                {
                    SessionHandler.Instance.currentCell = this;
                    playScript.LoadQuestion(cellValue, false, false);
                }
                break;
        /*
            case 1:
			//battle for the red cell
                if (playScript.challengeMode && SessionHandler.Instance.currentSession.redPlayerId != MainGameManager.Instance.LocalPlayerId)
                {
                    challengeConfirmation.GetComponent<RectTransform>().position = gameObject.GetComponent<RectTransform>().position;
                    challengeConfirmation.SetActive(true);
                    challengeConfirmation.GetComponent<RectTransform>().SetAsLastSibling();
                    SessionHandler.Instance.currentCell = this;
                }
                break;
            case 2:
			//battle for the blue cell
                if (playScript.challengeMode && SessionHandler.Instance.currentSession.redPlayerId == MainGameManager.Instance.LocalPlayerId)
                {
                    challengeConfirmation.GetComponent<RectTransform>().position = gameObject.GetComponent<RectTransform>().position;
                    challengeConfirmation.SetActive(true);
                    challengeConfirmation.GetComponent<RectTransform>().SetAsLastSibling();
                    SessionHandler.Instance.currentCell = this;
                }
                break;
            case 3:
			//battle for the black cell
                if (playScript.challengeMode)
                {
                    challengeConfirmation.GetComponent<RectTransform>().position = gameObject.GetComponent<RectTransform>().position;
                    challengeConfirmation.SetActive(true);
                    challengeConfirmation.GetComponent<RectTransform>().SetAsLastSibling();
                    SessionHandler.Instance.currentCell = this;
                }
                break;
                */
            default:
                Debug.Log("Error ocured. cellType: " + cellType);
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CellTapped();
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathChecker : MonoBehaviour {
	public GamePlay playScript;
	List<List<int[]>> allPaths;
	// Use this for initialization
	void Start () {
		//playScript =this.GetComponent<GamePlay> ();
		allPaths = new List<List<int[]>> ();
		//CreateRedPathFromCellandPath (0, 0, null, null);
		//CreateRedPathFromCellandPath (0, 1, null, null);
		//CreateRedPathFromCellandPath (0, 2, null, null);
		//CreateRedPathFromCellandPath (0, 3, null, null);
		//CheckRedPaths ();

		//CreateBluePathFromCellandPath (0, 0, null, null);
		//CreateBluePathFromCellandPath (1, 0, null, null);
		//CreateBluePathFromCellandPath (2, 0, null, null);
		//CreateBluePathFromCellandPath (3, 0, null, null);
		//CreateBluePathFromCellandPath (4, 0, null, null);
		//CheckBluePaths ();
	}

	#region red player paths
	void CreateRedPathFromCellandPath(int xCoord, int yCoord, List<int[]> curPath, int[] pseudoField)
	{
		if (curPath == null)
		{
			curPath = new List<int[]> ();
		}

		int cellValuesIndexR = xCoord + yCoord * 5;
		if (pseudoField == null)
		{
			if (playScript.cellValues [cellValuesIndexR] != 1)
				return;
		} else
		{
			if (pseudoField [cellValuesIndexR] != 1)
				return;
		}

		curPath.Add (new int[]{ xCoord, yCoord });
		allPaths.Add (curPath);
		List <int[]> neighboursOfCell = GetRedCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) {
			//if (!curPath.Contains (neighboursOfCell [neighbourInd])) 
			if (curPath.Count > 1) {
				//checks if array path already has cell
				bool alreadyHasTheCell = false;
				for (int coordIndex = 0; coordIndex < curPath.Count; coordIndex++)
				{
					if (curPath [coordIndex] [0] == neighboursOfCell [neighbourInd] [0] && curPath [coordIndex] [1] == neighboursOfCell [neighbourInd] [1])
						alreadyHasTheCell = true;
				}
				if (!alreadyHasTheCell)
				{
					int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
					if (pseudoField == null)
					{
						if (playScript.cellValues [cellValuesIndex] == 1) {
							CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, null);
						}
					} else
					{
						if (pseudoField [cellValuesIndex] == 1) {
							CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
						}
					}

				}
			} else {
				int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
				if (pseudoField == null)
				{
					if (playScript.cellValues [cellValuesIndex] == 1) {
						CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, null);
					}
				} else
				{
					if (pseudoField [cellValuesIndex] == 1) {
						CreateRedPathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
					}
				}
			}
		}
	}

	List<int[]> GetRedCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (yCoord > 0) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord - 1});
			if (xCoord < 4 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
		}
		if (yCoord < 3) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord + 1});
			if (xCoord%2 == 1 && xCoord < 4)
				neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
		}
		if (xCoord < 4)
			neighbours.Add (new int[]{ xCoord + 1, yCoord});
		return neighbours;
	}

	public void CheckRedPaths ()
	{
		bool hasWinningPath = false;
		List<int[]> largestPath = new List<int[]> ();
		foreach (List<int[]> path in allPaths) 
		{
			if (largestPath.Count == 0)
				largestPath = path;
//			if (path [path.Count - 1] [0] == 4) 
//			{
//				hasWinningPath = true;
//				largestPath = path;
//			} 
            else if (path [path.Count - 1] [0] > largestPath [largestPath.Count - 1] [0]) 
			{
				largestPath = path;
			} else if (path [path.Count - 1] [0] == largestPath [largestPath.Count - 1] [0] &&
				path.Count > largestPath.Count)
			{
				largestPath = path;
			}
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [0] == 4) 
                {
                    hasWinningPath = true;
                    largestPath = path;
                }
            }
		}

		//log largest path
		Debug.Log("Has winning path red:" + hasWinningPath + "\n");
		Debug.Log("Largest path blue:\n");
		for (int i = 0; i < largestPath.Count; i++) 
		{
			Debug.Log (largestPath [i] [0] + " " + largestPath [i] [1] + "\n");
		}
	}

	public bool IsThereWinningPathForRed ()
	{
		allPaths = new List<List<int[]>> ();
		CreateRedPathFromCellandPath (0, 0, null, null);
		CreateRedPathFromCellandPath (0, 1, null, null);
		CreateRedPathFromCellandPath (0, 2, null, null);
		CreateRedPathFromCellandPath (0, 3, null, null);
		bool hasWinningPath = false;
		foreach (List<int[]> path in allPaths) 
		{
//			if (path [path.Count - 1] [0] == 4) 
//			{
//				hasWinningPath = true;
//			}
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [0] == 4) 
                {
                    hasWinningPath = true;
                }
            }
		}
		return hasWinningPath;
	}
	#endregion

	#region blue player paths
	void CreateBluePathFromCellandPath(int xCoord, int yCoord, List<int[]> curPath, int[] pseudoField)
	{
		if (curPath == null)
		{
			curPath = new List<int[]> ();
		}

		int cellValuesIndexB = xCoord + yCoord * 5;
		if (pseudoField == null)
		{
			if (playScript.cellValues [cellValuesIndexB] != 2)
				return;
		} else
		{
			if (pseudoField [cellValuesIndexB] != 2)
				return;
		}

		curPath.Add (new int[]{ xCoord, yCoord });
		allPaths.Add (curPath);
		List <int[]> neighboursOfCell = GetBlueCellNeighbours (xCoord, yCoord);
		for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
		{
			//if (!curPath.Contains (neighboursOfCell [neighbourInd])) 
			if (curPath.Count > 1) 
			{
				//checks if array path already has cell
				bool alreadyHasTheCell = false;
				for (int coordIndex = 0; coordIndex < curPath.Count; coordIndex++)
				{
					if (curPath [coordIndex] [0] == neighboursOfCell [neighbourInd] [0] && curPath [coordIndex] [1] == neighboursOfCell [neighbourInd] [1])
						alreadyHasTheCell = true;
				}
				if (!alreadyHasTheCell)
				{
					int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
					if (pseudoField == null)
					{
						if (playScript.cellValues [cellValuesIndex] == 2) {
							CreateBluePathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, null);
						}
					} else
					{
						if (pseudoField [cellValuesIndex] == 2) {
							CreateBluePathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
						}
					}
				}
			} else 
			{
				int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
				if (pseudoField == null)
				{
					if (playScript.cellValues [cellValuesIndex] == 2) {
						CreateBluePathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, null);
					}
				} else
				{
					if (pseudoField [cellValuesIndex] == 2) {
						CreateBluePathFromCellandPath (neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], curPath, pseudoField);
					}
				}
			}
		}
	}

	List<int[]> GetBlueCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (xCoord > 0) 
		{
			neighbours.Add (new int[]{ xCoord - 1, yCoord});
			if (yCoord > 0 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord - 1, yCoord - 1});
			if (yCoord < 3 && xCoord%2 == 1)
				neighbours.Add (new int[]{ xCoord - 1, yCoord + 1});
		}
		if (xCoord < 4) 
		{
			if (yCoord > 0 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
			if (yCoord < 3 && xCoord%2 == 1)
				neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
			neighbours.Add (new int[]{ xCoord + 1, yCoord});
		}
		if (yCoord < 3) 
		{
			neighbours.Add (new int[]{ xCoord, yCoord + 1});
		}
		return neighbours;
	}

	public void CheckBluePaths ()
	{
		bool hasWinningPath = false;
		List<int[]> largestPath = new List<int[]> ();
		foreach (List<int[]> path in allPaths) 
		{
            
			if (largestPath.Count == 0)
				largestPath = path;
			//if (path [path.Count - 1] [1] == 3) 
			//{
			//	hasWinningPath = true;
			//	largestPath = path;
			//} 
            else if (path [path.Count - 1] [0] > largestPath [largestPath.Count - 1] [0]) 
			{
				largestPath = path;
			} else if (path [path.Count - 1] [0] == largestPath [largestPath.Count - 1] [0] &&
				path.Count > largestPath.Count)
			{
				largestPath = path;
			}
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [1] == 3) 
                {
                    hasWinningPath = true;
                    largestPath = path;
                }
            }
		}

		//log largest path
		Debug.Log("Has winning path blue:" + hasWinningPath + "\n");
		Debug.Log("Largest blue path:\n");
		for (int i = 0; i < largestPath.Count; i++) 
		{
			Debug.Log (largestPath [i] [0] + " " + largestPath [i] [1] + "\n");
		}
	}

	public bool IsThereWinningPathForBlue ()
	{
		allPaths = new List<List<int[]>> ();
		CreateBluePathFromCellandPath (0, 0, null, null);
		CreateBluePathFromCellandPath (1, 0, null, null);
		CreateBluePathFromCellandPath (2, 0, null, null);
		CreateBluePathFromCellandPath (3, 0, null, null);
		CreateBluePathFromCellandPath (4, 0, null, null);
		bool hasWinningPath = false;
		foreach (List<int[]> path in allPaths) 
		{
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [1] == 3) 
                {
                    hasWinningPath = true;
                }
            }
//			if (path [path.Count - 1] [1] == 3) 
//			{
//				hasWinningPath = true;
//			}
		}
		return hasWinningPath;
	}
	#endregion

	public bool IsThereWinningPath ()
	{
		allPaths = new List<List<int[]>> ();
		CreateBluePathFromCellandPath (0, 0, null, null);
		CreateBluePathFromCellandPath (1, 0, null, null);
		CreateBluePathFromCellandPath (2, 0, null, null);
		CreateBluePathFromCellandPath (3, 0, null, null);
		CreateBluePathFromCellandPath (4, 0, null, null);
		foreach (List<int[]> path in allPaths) 
		{
//			if (path [path.Count - 1] [1] == 3) 
//			{
//				return true;
//			}
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [1] == 3) 
                {
                    return true;
                }
            }
		}

		allPaths = new List<List<int[]>> ();
		CreateRedPathFromCellandPath (0, 0, null, null);
		CreateRedPathFromCellandPath (0, 1, null, null);
		CreateRedPathFromCellandPath (0, 2, null, null);
		CreateRedPathFromCellandPath (0, 3, null, null);
		foreach (List<int[]> path in allPaths) 
		{
//			if (path [path.Count - 1] [0] == 4) 
//			{
//				return true;
//			}
            for (int i = 0; i < path.Count; i++)
            {
                if (path [i] [0] == 4) 
                {
                    return true;
                }
            }
		}
		return false;
	}

	public bool WillWinInTwoCells (int xCoord, int yCoord)
	{
		Debug.Log ("playScript.cellValues.Length: " + playScript.cellValues.Length);
		int[] pseudoCellValues = new int[playScript.cellValues.Length];
		System.Array.Copy (playScript.cellValues, pseudoCellValues, playScript.cellValues.Length);
		int cellValuesIndex = xCoord + yCoord * 5;
        if (SessionHandler.Instance.currentSession.redPlayerId == MainGameManager.Instance.LocalPlayerId) 
		{
			Debug.Log ("Before:" + playScript.cellValues [cellValuesIndex]);
			pseudoCellValues [cellValuesIndex] = 1;
			Debug.Log ("After:" + playScript.cellValues [cellValuesIndex]);
			List <int[]> neighboursOfCell = GetRedCellNeighbours (xCoord, yCoord);
			for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) {
				if (pseudoCellValues [cellValuesIndex] == 0)
					pseudoCellValues [cellValuesIndex] = 1;
			}
			//check for winning path
			allPaths = new List<List<int[]>> ();
			CreateRedPathFromCellandPath (0, 0, null, pseudoCellValues);
			CreateRedPathFromCellandPath (0, 1, null, pseudoCellValues);
			CreateRedPathFromCellandPath (0, 2, null, pseudoCellValues);
			CreateRedPathFromCellandPath (0, 3, null, pseudoCellValues);
			foreach (List<int[]> path in allPaths) 
			{
//				if (path [path.Count - 1] [0] == 4) 
//				{
//					return true;
//				}
                for (int i = 0; i < path.Count; i++)
                {
                    if (path [i] [0] == 4) 
                    {
                        return true;
                    }
                }
			}
		}else
		{
			pseudoCellValues[cellValuesIndex] = 2;
			List <int[]> neighboursOfCell = GetBlueCellNeighbours (xCoord, yCoord);
			for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) {
				if (pseudoCellValues [cellValuesIndex] == 0)
					pseudoCellValues [cellValuesIndex] = 2;
			}
			//check for winning path
			allPaths = new List<List<int[]>> ();
			CreateBluePathFromCellandPath (0, 0, null, pseudoCellValues);
			CreateBluePathFromCellandPath (1, 0, null, pseudoCellValues);
			CreateBluePathFromCellandPath (2, 0, null, pseudoCellValues);
			CreateBluePathFromCellandPath (3, 0, null, pseudoCellValues);
			CreateBluePathFromCellandPath (4, 0, null, pseudoCellValues);
			foreach (List<int[]> path in allPaths) 
			{
//				if (path [path.Count - 1] [1] == 3) 
//				{
//					return true;
//				}
                for (int i = 0; i < path.Count; i++)
                {
                    if (path [i] [1] == 3) 
                    {
                        return true;
                    }
                }
			}
		}
		return false;
	}
}

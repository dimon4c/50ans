﻿using UnityEngine;
using System.Collections;

public class LoadChallenge : MonoBehaviour
{

    public void GoChallenge()
    {
        GamePlay playScript = FindObjectOfType<GamePlay>();
        playScript.LoadQuestion(SessionHandler.Instance.currentCell.cellValue, true, false);
        gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestionTab : MonoBehaviour
{
    public GamePlay playScript;
    public Keybord keyBoardScript;

    public GameObject challengeModeTitle;
    public GameObject redCellBack;
    public GameObject blueCellBack;
    public GameObject blackCellBack;

    public Text roundText;
    public Text categoryText;
    public Text questionText;

    public GameObject openQuestion;
    public GameObject letterBtn;
    public GameObject americanQuestion;
    public GameObject bombBtn;

    public Text bombBadgeText;
    public Text letterBadgeText;
    public Text switchBadgeText;
    public Text timerBadgeText;
    public GameObject flagActionButton;
    public Text flagsBadgeText;

    public Text coinsOnQuestTabText;
    public GameObject continueScreen;
    public Text continueTitle;

    public int questionType;
    //0- american questions, 1- open questions

    public AnswerButton answer1Button;
    public AnswerButton answer2Button;
    public AnswerButton answer3Button;
    public AnswerButton answer4Button;

    public GameObject WrongAnim;
    public GameObject CorrectAnim;

    public Animator questionAnimator;

    int questDifficulty;
    int rightAnswerNo;
    string rightAnswer;

    List<int> wrongAnswers;
    int removedWrongAnswers;

    public PathChecker checkPath;

    public Text timerText;
    int secondsForAnswer;

    int answersStreak;

    public void SetQuestion(bool fromQuestTab, bool challengeMode, int questionTypes, int questionDifficulty, int roundNumber, string category, 
                            string question, string answer1, string answer2, string answer3, string answer4,
                            string flag1, string flag2, string flag3, string flag4,
                            int rightAnswerNumber, string rightAnswerO)
    {
        rightAnswerO.ToUpper();
        flagActionButton.SetActive(false);
        questDifficulty = questionDifficulty;
        coinsOnQuestTabText.text = MainGameManager.Instance.PlayerCoins.ToString();
        challengeModeTitle.SetActive(challengeMode);
        switch (SessionHandler.Instance.currentCell.cellType)
        {
            case 1:
                redCellBack.SetActive(true);
                blueCellBack.SetActive(false);
                blackCellBack.SetActive(false);
                break;
            case 2:
                redCellBack.SetActive(false);
                blueCellBack.SetActive(true);
                blackCellBack.SetActive(false);
                break;
            case 3:
                redCellBack.SetActive(false);
                blueCellBack.SetActive(false);
                blackCellBack.SetActive(true);
                break;
            default:
                redCellBack.SetActive(false);
                blueCellBack.SetActive(false);
                blackCellBack.SetActive(false);
                break;
        }
        roundText.text = "Round " + roundNumber;
        categoryText.text = category;
        questionText.text = question;

        questionType = questionTypes;
        switch (questionType)
        {
            case 1: //open question type
                americanQuestion.SetActive(false);
                bombBtn.SetActive(false);
                openQuestion.SetActive(true);
                letterBtn.SetActive(true);
                rightAnswer = rightAnswerO;
                rightAnswerNo = 1;
                keyBoardScript.CreateWord(rightAnswerO);
                break;
            default: //american question type
                americanQuestion.SetActive(true);
                bombBtn.SetActive(true);
                openQuestion.SetActive(false);
                letterBtn.SetActive(false);
                answer1Button.SetAnswer(answer1);
                answer1Button.HideFlag();
                answer2Button.SetAnswer(answer2);
                answer2Button.HideFlag();
                answer3Button.SetAnswer(answer3);
                answer3Button.HideFlag();
                answer4Button.SetAnswer(answer4);
                answer4Button.HideFlag();
                //TODO: set flags for buttons
                if (flag1 != null)
                {
                    flagActionButton.SetActive(true);
                    answer1Button.hasFlag = true;
                    answer2Button.hasFlag = true;
                    answer3Button.hasFlag = true;
                    answer4Button.hasFlag = true;
                }
                rightAnswerNo = rightAnswerNumber;
                Debug.Log("Right answer no: " + rightAnswerNo);
                wrongAnswers = new List<int>();
                wrongAnswers.Add(1);
                wrongAnswers.Add(2);
                wrongAnswers.Add(3);
                wrongAnswers.Add(4);
                wrongAnswers.Remove(rightAnswerNumber);
                break;
        }


        removedWrongAnswers = 0;

        if (fromQuestTab)
        {
            secondsForAnswer += MainGameManager.Instance.TimeToAddSwitch;
        }
        else
        {
            bombBadgeText.text = playScript.numberOfFreeBombs.ToString();
            letterBadgeText.text = playScript.numberOfFreeLetters.ToString();
            switchBadgeText.text = playScript.numberOfFreeSwitches.ToString();
            timerBadgeText.text = playScript.numberOfFreeTimers.ToString();
            flagsBadgeText.text = playScript.numberOfFreeTimers.ToString();
            secondsForAnswer = MainGameManager.Instance.TimeToAnswer;
            questionAnimator.SetTrigger("ShowQuestion");
            StartCoroutine("TimerCountdown");
        }
    }

    private IEnumerator TimerCountdown()
    {
        while (secondsForAnswer > 0)
        {
            secondsForAnswer--;
            int minutes = Mathf.FloorToInt(secondsForAnswer / 60F);
            int seconds = Mathf.FloorToInt(secondsForAnswer - minutes * 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
            yield return new WaitForSeconds(1.0f);
        }
        StopCoroutine("TimerCountdown");
        //show out of time
        if (playScript.challengeMode)
        {
            playScript.challengeMode = false;
        }
        else
        {
            if (playScript.isThereCellToAnswer)
            {
                playScript.isThereCellToAnswer = false;
                //SessionHandler.Instance.currentCell.SetCellType(3);
                int cellValueIndex = SessionHandler.Instance.currentCell.cellX + SessionHandler.Instance.currentCell.cellY * 5;
                playScript.cellValues[cellValueIndex] = 3;
            }
            else
            {
                playScript.isThereCellToAnswer = true;
                playScript.answerCellX = SessionHandler.Instance.currentCell.cellX;
                playScript.answerCellY = SessionHandler.Instance.currentCell.cellY;
            }
        }

        playScript.turnEnded = true;
        if (SessionHandler.Instance.currentSession.redPlayerId != MainGameManager.Instance.LocalPlayerId)
            playScript.roundNo++;

        MainGameManager.Instance.WrongAnswers++;
        continueTitle.text = "Out of time!";
        continueScreen.SetActive(true);
    }

    public void AnswerQuestion(int answerNo)
    {
        StopCoroutine("TimerCountdown");
        playScript.loadingQuestion = false;
        playScript.numberOfPlayerAnswers++;
        if (answerNo == rightAnswerNo)
        {
            int typeToChange = 0;
            if (SessionHandler.Instance.currentSession.redPlayerId == MainGameManager.Instance.LocalPlayerId)
                typeToChange = 1;
            else
                typeToChange = 2;

            answersStreak++;

            switch (answerNo)
            {
                case 1:
                    answer1Button.SetCorrect();
                    break;
                case 2:
                    answer2Button.SetCorrect();
                    break;
                case 3:
                    answer3Button.SetCorrect();
                    break;
                case 4:
                    answer4Button.SetCorrect();
                    break;
            }
            CorrectAnim.SetActive(true);
			
            //SessionHandler.Instance.currentCell.SetCellType(typeToChange);
            int cellValueIndex = SessionHandler.Instance.currentCell.cellX + SessionHandler.Instance.currentCell.cellY * 5;
            playScript.cellValues[cellValueIndex] = typeToChange;
            playScript.pathDrawScript.DrawPath();
            playScript.numberOfRightPlayerAnswers++;
            MainGameManager.Instance.CorrectAnswers++;
            switch (questDifficulty)
            {
                case 1:
                    MainGameManager.Instance.CorrectAnswersLevel1++;
                    break;
                case 2:
                    MainGameManager.Instance.CorrectAnswersLevel2++;
                    break;
                case 3:
                    MainGameManager.Instance.CorrectAnswersLevel3++;
                    break;
                case 4:
                    MainGameManager.Instance.CorrectAnswersLevel4++;
                    break;
            }

            //TODO: add correct animation
            //check if win the game
            //if not win:
            //and check for blue if blue player
            if (SessionHandler.Instance.currentSession.redPlayerId == MainGameManager.Instance.LocalPlayerId)
            {
                if (checkPath.IsThereWinningPathForRed())
                {
                    Debug.Log("red wins!");
                    MainGameManager.Instance.GameWins++;
                    continueTitle.text = "You win!!!";
                    continueScreen.SetActive(true);
                    playScript.gameFinished = true;
                    playScript.redWon = true;
                }
                else
                    Invoke("CloseQuestionCorrect", 3f);
            }
            else
            {
                if (checkPath.IsThereWinningPathForBlue())
                {
                    Debug.Log("blue wins!");
                    MainGameManager.Instance.GameWins++;
                    continueTitle.text = "You win!!!";
                    continueScreen.SetActive(true);
                    playScript.gameFinished = true;
                }
                else
                    Invoke("CloseQuestionCorrect", 3f);
            }
            playScript.challengeMode = false;
        }
        else
        {
            if (answersStreak > MainGameManager.Instance.AnswerStreak)
                MainGameManager.Instance.AnswerStreak = answersStreak;
            answersStreak = 0;

            switch (answerNo)
            {
                case 1:
                    answer1Button.SetWrong();
                    break;
                case 2:
                    answer2Button.SetWrong();
                    break;
                case 3:
                    answer3Button.SetWrong();
                    break;
                case 4:
                    answer4Button.SetWrong();
                    break;
            }
            WrongAnim.SetActive(true);

            if (playScript.challengeMode)
            {
                playScript.challengeMode = false;
            }
            else
            {
                if (playScript.isThereCellToAnswer)
                {
                    playScript.isThereCellToAnswer = false;
                    //SessionHandler.Instance.currentCell.SetCellType(3);
                    int cellValueIndex = SessionHandler.Instance.currentCell.cellX + SessionHandler.Instance.currentCell.cellY * 5;
                    playScript.cellValues[cellValueIndex] = 3;
                }
                else
                {
                    playScript.isThereCellToAnswer = true;
                    playScript.answerCellX = SessionHandler.Instance.currentCell.cellX;
                    playScript.answerCellY = SessionHandler.Instance.currentCell.cellY;
                }
            }

            playScript.turnEnded = true;
            if (SessionHandler.Instance.currentSession.redPlayerId != MainGameManager.Instance.LocalPlayerId)
                playScript.roundNo++;

            MainGameManager.Instance.WrongAnswers++;
            switch (questDifficulty)
            {
                case 1:
                    MainGameManager.Instance.WrongAnswersLevel1++;
                    break;
                case 2:
                    MainGameManager.Instance.WrongAnswersLevel2++;
                    break;
                case 3:
                    MainGameManager.Instance.WrongAnswersLevel3++;
                    break;
                case 4:
                    MainGameManager.Instance.WrongAnswersLevel4++;
                    break;
            }

            //TODO: add wrong animation
//			continueTitle.text = "Wrong answer!";
//            continueScreen.SetActive (true);
            Invoke("CloseQuestionWrong", 3f);
        }
        //animate back to field or show continue screen with text wrong answer and button to home screen
        //save game state
    }

    void CloseQuestionWrong()
    {
        WrongAnim.SetActive(false);
        playScript.GoHomeScreen();
    }

    void CloseQuestionCorrect()
    {
        CorrectAnim.SetActive(false);
        questionAnimator.SetTrigger("HideQuestion");
    }

    public void TimerAction()
    {
        if (playScript.numberOfFreeTimers > 0)
        {
            playScript.numberOfFreeTimers--;
            timerBadgeText.text = playScript.numberOfFreeTimers.ToString();
            secondsForAnswer += MainGameManager.Instance.TimeToAdd;
            int minutes = Mathf.FloorToInt(secondsForAnswer / 60F);
            int seconds = Mathf.FloorToInt(secondsForAnswer - minutes * 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
            //check if more than 1 ???
        }
        else
        {
            //buy more
        }
    }

    public void SwitchAction()
    {
        if (playScript.numberOfFreeSwitches > 0)
        {
            playScript.numberOfFreeSwitches--;
            switchBadgeText.text = playScript.numberOfFreeSwitches.ToString();
            //replace question
            //check if more than 1 ???
            playScript.LoadQuestion(categoryText.text, playScript.challengeMode, true);
        }
        else
        {
            //buy more
        }
    }

    public void LetterAddAction()
    {
        if (playScript.numberOfFreeLetters > 0)
        {
            playScript.numberOfFreeLetters--;
            letterBadgeText.text = playScript.numberOfFreeLetters.ToString();
            //add random letter from right answer
            //check if more than 1 ???
            keyBoardScript.OpenALetter();
        }
        else
        {
            //buy more
        }
    }

    public void FlagsShowAction()
    {
        if (playScript.numberOfFreeTimers > 0) //timers just abstract third action
        {
            playScript.numberOfFreeTimers--;
            flagsBadgeText.text = playScript.numberOfFreeTimers.ToString();
            //shows answer flags
            answer1Button.ShowFlag();
            answer2Button.ShowFlag();
            answer3Button.ShowFlag();
            answer4Button.ShowFlag();
        }
        else
        {
            //buy more
        }
    }

    public void RemoveWrongAnswer()
    {
        if (playScript.numberOfFreeBombs > 0)
        {
            if (removedWrongAnswers < 2)
            {
                playScript.numberOfFreeBombs--;
                bombBadgeText.text = playScript.numberOfFreeBombs.ToString();
                if (MainGameManager.Instance.PlayerCoins > 0)
                {
                    int randWrongAnswer = 0;
                    if (wrongAnswers.Count > 0)
                    {
                        randWrongAnswer = wrongAnswers[Random.Range(0, wrongAnswers.Count)];
                        switch (randWrongAnswer)
                        {
                            case 1:
                                answer1Button.SetNonActive();
                                break;
                            case 2:
                                answer2Button.SetNonActive();
                                break;
                            case 3:
                                answer3Button.SetNonActive();
                                break;
                            case 4:
                                answer4Button.SetNonActive();
                                break;
                        }
                        wrongAnswers.Remove(randWrongAnswer);
                        removedWrongAnswers++;
                        MainGameManager.Instance.PlayerCoins--;
                    }
                }
                else
                {
                    //show more coins screen
                }
            }
        }
        else
        {
            //buy more
        }
    }
}

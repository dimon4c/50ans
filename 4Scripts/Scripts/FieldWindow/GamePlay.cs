﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;
using System;
using System.Collections.Generic;

public class GamePlay : MonoBehaviour
{
    private int questionType = 1;
    public QuestionTab questionTabScript;
    public DrawPaths pathDrawScript;
    public Cell[] cells;
    public string[] cellCategories;
    public int[] cellValues;

    public Text bluePlayerNameText;
    public Text redPlayerNameText;
    public Image bluePlayerPhoto;
    public Image redPlayerPhoto;
    public Text coinsText;
    public Text cashText;
    public Text redStats;
    public Text blueStats;

    public bool playersTurnStarted;

    public bool gameFinished;
    public bool redWon;
    public bool turnEnded;
    public bool isThereCellToAnswer;
    public int answerCellX;
    public int answerCellY;

    public int numberOfPlayerAnswers;
    public int numberOfRightPlayerAnswers;
    public string playerBestLetter;
    public string playerWorseLetter;

    public int numberOfFreeShuffle;
    public Text shuffleBadge;
    public int numberOfFreeChallenge;
    public Text challengeBadge;

    public int numberOfFreeBombs;
    public int numberOfFreeLetters;
    public int numberOfFreeSwitches;
    public int numberOfFreeTimers;

    public bool challengeMode = false;
    public bool loadingQuestion = false;

    public int roundNo;

    List<int> questionIndexes;

    public GameObject noQuestWindow;
    public Transform cellsField;
    public GameObject fadeObject;
    public RectTransform popupBack;
    public RectTransform trianglePopup;
    public RectTransform triangleAnchor;

    PathChecker checkPath;

    QuestionsArray allQuestions;
    //List<QuestionFromJson> allQuestions;

    public Sprite[] categoriesSprites;

    // Use this for initialization
    void Start()
    {
        MainGameManager.Instance.SavedGameId = "";
        numberOfFreeBombs = 1;
        numberOfFreeLetters = 1; 
        numberOfFreeSwitches = 1;
        numberOfFreeTimers = 1;

        allQuestions = JsonUtility.FromJson<QuestionsArray>(MainGameManager.Instance.LoadQuestions("English"));
        checkPath = this.GetComponent<PathChecker>();

        questionIndexes = new List<int>();
        PopulateGameScreen();
        if (isThereCellToAnswer)
        {
            //ShowCellToAnswer();
        }
    }
    /*
    void ShowCellToAnswer()
    {
        fadeObject.SetActive(true);
        foreach (Cell cellScript in cells)
        {
            if (cellScript.cellX == answerCellX && cellScript.cellY == answerCellY)
            {
                SessionHandler.Instance.currentCell = cellScript;
                cellScript.transform.SetParent(fadeObject.transform);
                trianglePopup.position = cellScript.GetComponent<RectTransform>().position;
                triangleAnchor.localScale = Vector3.one;
                float popupYPos = 191f;
                if (cellScript.cellY < 2)
                {
                    triangleAnchor.localScale = new Vector3(1, -1, 1);
                    popupYPos = -189f;
                }
                popupBack.localPosition = new Vector2(0, popupYPos);

                popupBack.SetParent(cellsField.transform);
                popupBack.localPosition = new Vector2(0, popupBack.localPosition.y);
                popupBack.SetParent(trianglePopup.transform);
                trianglePopup.gameObject.SetActive(true);
            }
        }
    }
    */
    public void LoadCellToAnswer()
    {
        LoadQuestion(SessionHandler.Instance.currentCell.cellValue, false, false);
        SessionHandler.Instance.currentCell.transform.SetParent(cellsField);
        fadeObject.SetActive(false);
        trianglePopup.gameObject.SetActive(false);
    }

    void PopulateGameScreen()
    {
        coinsText.text = MainGameManager.Instance.PlayerCoins.ToString();
        cashText.text = MainGameManager.Instance.PlayerMoney.ToString();
        bluePlayerNameText.text = "Your opponent";
        redPlayerNameText.text = MainGameManager.Instance.LocalPlayerName;

        Game currentGame = SessionHandler.Instance.currentSession;
        if (currentGame != null)
        {
            cellValues = currentGame.cellValues;
            cellCategories = currentGame.cellCategories;
            /*
            foreach (Cell cellSccript in cells)
            {
                
                int cellCategoryIndex = cellSccript.cellX + cellSccript.cellY * 5;
                cellSccript.SetCellValue(cellCategories[cellCategoryIndex], false, GetSpriteForCategory(cellCategories[cellCategoryIndex]));
                //cellSccript.SetCellType(cellValues[cellCategoryIndex]);
            }
*/
            bluePlayerNameText.text = currentGame.bluePlayerName;
            redPlayerNameText.text = currentGame.redPlayerName;

            if (currentGame.redPlayerId == MainGameManager.Instance.LocalPlayerId)
            {
                numberOfPlayerAnswers = currentGame.redPlayerAnswersNo;
                numberOfRightPlayerAnswers = currentGame.redPlayerRightAnswersNo;
                playerBestLetter = currentGame.redPlayerWorseLetter;
                playerWorseLetter = currentGame.redPlayerBestLetter;
                numberOfFreeShuffle = currentGame.redNoOfFreeShuffle;
                numberOfFreeChallenge = currentGame.redNoOfFreeChallenge;
            }
            else
            {
                numberOfPlayerAnswers = currentGame.bluePlayerAnswersNo;
                numberOfRightPlayerAnswers = currentGame.bluePlayerRightAnswersNo;
                playerBestLetter = currentGame.bluePlayerWorseLetter;
                playerWorseLetter = currentGame.bluePlayerBestLetter;
                numberOfFreeShuffle = currentGame.blueNoOfFreeShuffle;
                numberOfFreeChallenge = currentGame.blueNoOfFreeChallenge;
            }

            isThereCellToAnswer = currentGame.isThereAnswerCell;
            answerCellX = currentGame.cellToAnswerX;
            answerCellY = currentGame.cellToAnswerY;

            roundNo = currentGame.roundNo;

            shuffleBadge.text = numberOfFreeShuffle.ToString();
            challengeBadge.text = numberOfFreeChallenge.ToString();
        }
        else
        {
            // new game
            //for random first letters
            //string[] alphabet = new string[]{"A","B","C","D","E","F","G", "H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
            cellCategories = new string[]{ "2", "2", "2", "2", "2", "2", "2", "2" };
            cellValues = new int[]{ 0, 0, 0, 0, 0, 0, 0, 0 };

            /*
            foreach (Cell cellSccript in cells)
            {
                int cellCategoryIndex = cellSccript.cellX + cellSccript.cellY * 5;
                //string randLetter = alphabet[UnityEngine.Random.Range (0, 26)];
                cellSccript.SetCellValue(cellCategories[cellCategoryIndex], false, null);
                //cellSccript.SetCellType(cellValues[cellCategoryIndex]);
            }
*/
            numberOfPlayerAnswers = 0;
            numberOfRightPlayerAnswers = 0;
            playerBestLetter = " ";
            playerWorseLetter = " ";
            numberOfFreeShuffle = 1;
            numberOfFreeChallenge = 1;

            isThereCellToAnswer = false;
            answerCellX = 0;
            answerCellY = 0;

            roundNo = 1;
            currentGame = CreateNewGameState();

            //rematch
            if (MainGameManager.Instance.OpponentId != "")
            {
                currentGame.bluePlayerId = MainGameManager.Instance.OpponentId;
                currentGame.bluePlayerName = MainGameManager.Instance.OpponentName;
                currentGame.bluePlayerPhotoLink = MainGameManager.Instance.OpponentPhotoLink;
                MainGameManager.Instance.OpponentId = "";
                bluePlayerNameText.text = currentGame.bluePlayerName;
            }
            SessionHandler.Instance.currentSession = currentGame;
        }
        UpdateStats();
        StartCoroutine(SetUserPhoto(bluePlayerPhoto, currentGame.bluePlayerPhotoLink));
        StartCoroutine(SetUserPhoto(redPlayerPhoto, currentGame.redPlayerPhotoLink));
    }

    IEnumerator SetUserPhoto(Image photoImage, string photoURL)
    {
        if (photoURL == " ")
            yield break;
        //Debug.Log("loading photo from:" + photoURL);
        WWW www = new WWW(photoURL);
        yield return www; 
        //Debug.Log("ASSIGNING PHOTO!");
        Sprite sprite = new Sprite();
        sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        photoImage.sprite = sprite;
    }

    string GetCategoryForPercent(float percentValue)
    {
        int numberOfGeneralQuestions = 0;
        int numberOfAthleticsQuestions = 0;
        int numberOfBasketballQuestions = 0;
        for (int questIndex = 0; questIndex < allQuestions.questionsArray.Count; questIndex++)
        {
            QuestionFromJson question = allQuestions.questionsArray[questIndex];
           
            if (question.category == 1)
                numberOfGeneralQuestions++;
            if (question.category == 2)
                numberOfAthleticsQuestions++;
            /*
            if (question.category == 3)
                numberOfBasketballQuestions++;
*/
        }
        float percentOfGeneralQuestions = numberOfGeneralQuestions / allQuestions.questionsArray.Count * 100.0f;
        float percentOfAthleticsQuestions = numberOfAthleticsQuestions / allQuestions.questionsArray.Count * 100.0f;
        // float percentOfBasketballQuestions = numberOfBasketballQuestions / allQuestions.questionsArray.Count * 100.0f;
        if (percentValue <= percentOfGeneralQuestions)
            return "General";
        else if (percentValue <= percentOfGeneralQuestions + percentOfAthleticsQuestions)
            return "Athletics";
        /*
        else if (percentValue <= percentOfGeneralQuestions + percentOfAthleticsQuestions  + percentOfBasketballQuestions)
            return "Basketball";
            //*/
        return "General";
    }

    Sprite GetSpriteForCategory(string category)
    {
        foreach (Sprite catSprite in categoriesSprites)
        {
            if (catSprite.name == category.ToString())
                return catSprite;
        }
        return null;
    }

    void UpdateStats()
    {
        float percentOfRightAnswers = 100F * SessionHandler.Instance.currentSession.redPlayerRightAnswersNo / SessionHandler.Instance.currentSession.redPlayerAnswersNo;
        Debug.Log("percentOfRightAnswers: " + percentOfRightAnswers);
        redStats.text = "% of right answers:" + percentOfRightAnswers.ToString("F0") + "\n" +
        "Best letter: " + SessionHandler.Instance.currentSession.redPlayerBestLetter + "\n" +
        "Worse letter: " + SessionHandler.Instance.currentSession.redPlayerWorseLetter;
        percentOfRightAnswers = 100F * SessionHandler.Instance.currentSession.bluePlayerRightAnswersNo / SessionHandler.Instance.currentSession.bluePlayerAnswersNo;
        blueStats.text = "% of right answers:" + percentOfRightAnswers.ToString("F0") + "\n" +
        "Best letter: " + SessionHandler.Instance.currentSession.bluePlayerBestLetter + "\n" +
        "Worse letter: " + SessionHandler.Instance.currentSession.bluePlayerWorseLetter;
    }

    public static string GetUniqueID()
    {
        var random = new System.Random();                     
        DateTime epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
        double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;

        string uniqueID = Application.systemLanguage//Language
                          + "-" + "mobile"//Device    
                          + "-" + String.Format("{0:X}", Convert.ToInt32(timestamp))//Time
                          + "-" + String.Format("{0:X}", Convert.ToInt32(Time.time * 1000000))//Time in game
                          + "-" + String.Format("{0:X}", random.Next(1000000000));                //random number

        Debug.Log("Generated Unique ID: " + uniqueID);
        return uniqueID;
    }


    Game CreateNewGameState()
    {
        Game someNewGame = new Game();
        someNewGame.gameId = GetUniqueID();
        someNewGame.gameStarted = System.DateTime.UtcNow.ToString();
        someNewGame.turn = "red";

        someNewGame.redPlayerName = MainGameManager.Instance.LocalPlayerName;
        someNewGame.redPlayerId = MainGameManager.Instance.LocalPlayerId;
        someNewGame.redPlayerPhotoLink = MainGameManager.Instance.LocalPlayerPhoto;
        someNewGame.redPlayerAnswersNo = 0;
        someNewGame.redPlayerRightAnswersNo = 0;
        someNewGame.redPlayerBestLetter = " ";
        someNewGame.redPlayerWorseLetter = " ";
        someNewGame.redNoOfFreeShuffle = 1;
        someNewGame.redNoOfFreeChallenge = 1;

        someNewGame.bluePlayerName = "Your opponent";
        someNewGame.bluePlayerId = " ";
        someNewGame.bluePlayerPhotoLink = " ";
        someNewGame.bluePlayerAnswersNo = 0;
        someNewGame.bluePlayerRightAnswersNo = 0;
        someNewGame.bluePlayerBestLetter = " ";
        someNewGame.bluePlayerWorseLetter = " ";
        someNewGame.blueNoOfFreeShuffle = 1;
        someNewGame.blueNoOfFreeChallenge = 1;

        someNewGame.roundNo = roundNo;
        someNewGame.cellValues = cellValues;
        someNewGame.cellCategories = cellCategories;
        return someNewGame;
    }

    Game SaveCurGameState()
    {
        //Game gameToSave = new Game ();
        Game gameToSave = SessionHandler.Instance.currentSession;
        //gameToSave.gameId = SessionHandler.Instance.currentSession.gameId;
        //gameToSave.timeOfStart = SessionHandler.Instance.currentSession.timeOfStart;
        gameToSave.lastTurn = System.DateTime.UtcNow.ToString();
        gameToSave.finished = gameFinished;
        if (gameToSave.finished)
        {
            //if (redWon && gameToSave.localPlayerRed)
            gameToSave.redWins = redWon;
        }
        if (turnEnded)
        {
            if (gameToSave.turn == "blue")
                gameToSave.turn = "red";
            else
                gameToSave.turn = "blue";
        }
        if (turnEnded && isThereCellToAnswer)
        {
            gameToSave.isThereAnswerCell = true;
            gameToSave.cellToAnswerX = answerCellX;
            gameToSave.cellToAnswerY = answerCellY;
        }
        else
        {
            gameToSave.isThereAnswerCell = false;
        }

        if (gameToSave.redPlayerId == MainGameManager.Instance.LocalPlayerId)
        {
            gameToSave.redPlayerAnswersNo = numberOfPlayerAnswers;
            gameToSave.redPlayerRightAnswersNo = numberOfRightPlayerAnswers;
            gameToSave.redPlayerBestLetter = playerBestLetter;
            gameToSave.redPlayerWorseLetter = playerWorseLetter;
            gameToSave.redNoOfFreeChallenge = numberOfFreeChallenge;
            gameToSave.redNoOfFreeShuffle = numberOfFreeShuffle;
        }
        else
        {
            gameToSave.bluePlayerAnswersNo = numberOfPlayerAnswers;
            gameToSave.bluePlayerRightAnswersNo = numberOfRightPlayerAnswers;
            gameToSave.bluePlayerBestLetter = playerBestLetter;
            gameToSave.bluePlayerWorseLetter = playerWorseLetter;
            gameToSave.blueNoOfFreeChallenge = numberOfFreeChallenge;
            gameToSave.blueNoOfFreeShuffle = numberOfFreeShuffle;
        }

        gameToSave.roundNo = roundNo;
        gameToSave.cellValues = cellValues;
        gameToSave.cellCategories = cellCategories;
        return gameToSave;
    }

    public void ShuffleAction()
    {
        if (numberOfFreeShuffle > 0)
        {
            numberOfFreeShuffle--;
            shuffleBadge.text = numberOfFreeShuffle.ToString();

            ShuffleBoard();

            while (checkPath.IsThereWinningPath())
            {
                ShuffleBoard();
            }
            /*
            foreach (Cell cellSccript in cells)
            {
                int cellCategoryIndex = cellSccript.cellX + cellSccript.cellY * 5;
                cellSccript.SetCellValue(cellCategories[cellCategoryIndex], false, GetSpriteForCategory(cellCategories[cellCategoryIndex]));
                //cellSccript.SetCellType(cellValues[cellCategoryIndex]);
            }
            pathDrawScript.DrawPath();
            */
        }
        else
        {
            //buy more shuffles
        }

    }

    void ShuffleBoard()
    {
        for (int indexOfArray = 0; indexOfArray < cellCategories.Length; indexOfArray++)
        {
            int randomIndex = UnityEngine.Random.Range(indexOfArray, cellCategories.Length);
            string tempStr = cellCategories[indexOfArray];
            cellCategories[indexOfArray] = cellCategories[randomIndex];
            cellCategories[randomIndex] = tempStr;
            int tempInt = cellValues[indexOfArray];
            cellValues[indexOfArray] = cellValues[randomIndex];
            cellValues[randomIndex] = tempInt;
        }
    }

    public void ShowChallengeOptions()
    {
        if (challengeMode)
        {
            challengeMode = false;
            numberOfFreeChallenge++;
            challengeBadge.text = numberOfFreeChallenge.ToString();
            foreach (Cell cellScript in cells)
            {
                // cellScript.DehighlightCell();
            }
        }
        else
        {
            if (numberOfFreeChallenge > 0)
            {
                numberOfFreeChallenge--;
                challengeBadge.text = numberOfFreeChallenge.ToString();

                challengeMode = true;
                //TODO: add button outside board to cancel challenge

                int targetCellType = 1;
                if (SessionHandler.Instance.currentSession.redPlayerId == MainGameManager.Instance.LocalPlayerId)
                    targetCellType = 2;
                //highlight challenge cells
                foreach (Cell cellScript in cells)
                {
                    if (cellScript.cellType == 3 || cellScript.cellType == targetCellType)
                    {
                        //cellScript.HighlightCell();
                    }
                }
            }
            else
            {
                //buy more challenges
            }
        }
    }

    int GetQuestionDifficulty()
    {
        //check if there is 2 cells for win
        int xCoord = SessionHandler.Instance.currentCell.cellX;
        int yCoord = SessionHandler.Instance.currentCell.cellY;
        if (checkPath.WillWinInTwoCells(xCoord, yCoord))
        {
            if (MainGameManager.Instance.LocalPlayerLevel == 4)
                return 4;
            else
                return MainGameManager.Instance.LocalPlayerLevel + 1;
        }
        else if (roundNo == 1)
        {
            if (MainGameManager.Instance.LocalPlayerLevel == 1)
                return 1;
            else
                return MainGameManager.Instance.LocalPlayerLevel - 1;
        }
        else
            return MainGameManager.Instance.LocalPlayerLevel;
    }

    public void LoadQuestion(string firstLetter, bool challenge, bool fromQuestTab)
    {
        playersTurnStarted = true;
        loadingQuestion = true;
        questionIndexes.Clear();

        int curDifficulty = GetQuestionDifficulty();
        Debug.Log("current difficulty: " + curDifficulty.ToString());

        for (int questIndex = 0; questIndex < allQuestions.questionsArray.Count; questIndex++)
        {
            QuestionFromJson question = allQuestions.questionsArray[questIndex];
            if (question.difficulty == curDifficulty)
            {
                String category = question.category.ToString();
                if (category.StartsWith(firstLetter, true, null))
                {
                    questionIndexes.Add(questIndex);
                }
            }
                
        }

        /*
        var allQuestions = JObject.Parse(MainGameManager.Instance.LoadQuestions("en"));
		for (int questIndex = 0; questIndex < allQuestions.AsArray.Count; questIndex++) 
		{
			if (allQuestions [questIndex] ["difficulty"].AsInt == curDifficulty) 
			{
				String rightAnswer = "";
				if (questionType == 1) //open question 
				{
					rightAnswer = allQuestions [questIndex] ["answer"];
				} else
					rightAnswer = allQuestions [questIndex] ["right_answer"];
				//Debug.Log (rightAnswer + rightAnswer[0]);
				if (rightAnswer.StartsWith (firstLetter, true, null)) 
				{
					if (questionType == 1) 
					{
						//checking answer < 19 characters
						if (rightAnswer.Length < 19)
							questionIndexes.Add (questIndex);
					} else
						questionIndexes.Add (questIndex);
				}
			}
		}
        */

        if (questionIndexes.Count > 0)
        {
            //*
            int choosenQuestionIndex = questionIndexes[UnityEngine.Random.Range(0, questionIndexes.Count)];
            string rightAnswer = "";
            if (questionType == 1)
                /*
            { //american type question
                int rightAnswerNo = 0;
                switch (allQuestions.questionsArray[choosenQuestionIndex].RightAnswer)
                {
                    case "a":
                        rightAnswerNo = 1;
                        break;
                    case "b":
                        rightAnswerNo = 2;
                        break;
                    case "c":
                        rightAnswerNo = 3;
                        break;
                    case "d":
                        rightAnswerNo = 4;
                        break;
                }

                questionTabScript.SetQuestion(fromQuestTab, challenge, questionType, curDifficulty, roundNo, firstLetter, 
                    allQuestions.questionsArray[choosenQuestionIndex].Question, 
                    allQuestions.questionsArray[choosenQuestionIndex].AnswerA, 
                    allQuestions.questionsArray[choosenQuestionIndex].AnswerB, 
                    allQuestions.questionsArray[choosenQuestionIndex].AnswerC, 
                    allQuestions.questionsArray[choosenQuestionIndex].AnswerD,
                    allQuestions.questionsArray[choosenQuestionIndex].FlagA,
                    allQuestions.questionsArray[choosenQuestionIndex].FlagB,
                    allQuestions.questionsArray[choosenQuestionIndex].FlagC,
                    allQuestions.questionsArray[choosenQuestionIndex].FlagD,
                    rightAnswerNo, rightAnswer);
            }
            else
            */
            {
                questionTabScript.SetQuestion(fromQuestTab, challenge, questionType, curDifficulty, roundNo, firstLetter, 
                    allQuestions.questionsArray[choosenQuestionIndex].question, 
                    "", "", "", "", null, null, null, null, 0, allQuestions.questionsArray[choosenQuestionIndex].answer);
                Debug.Log("right answer: " + allQuestions.questionsArray[choosenQuestionIndex].answer);
            }

            //TODO: save game state (current question and scene and choosen cell)

            //delete question from db and save db to documents
            allQuestions.questionsArray.Remove(allQuestions.questionsArray[choosenQuestionIndex]);
            MainGameManager.Instance.SaveQuestions(allQuestions.ToString(), "English");
            //*/
        }
        else
        {
            Debug.Log("show no questions screen");
            noQuestWindow.SetActive(true);
        }
    }

    public void HideNoQuestWin()
    {
        loadingQuestion = false;
        noQuestWindow.SetActive(false);
    }


    void SaveGameState(bool saveId)
    {
        if (playersTurnStarted)
        {
            //notify homescreen to save game to storage
            PlayerPrefs.SetInt("gameToUpdate", 1);
            SessionHandler.Instance.currentSession = SaveCurGameState();
            MainGameManager.Instance.SaveGames(MainGameManager.Instance.LoadGames(), SessionHandler.Instance.currentSession);
        }
        if (saveId)
            MainGameManager.Instance.SavedGameId = SessionHandler.Instance.currentSession.gameId;
    }

    public void GoHomeScreen()
    {
        SaveGameState(false);
        SceneManager.LoadScene("HomeScreen");
    }

    void OnApplicationPause(bool pause)
    {
        if (pause) // we are in background
        {
            Debug.Log("paused");
            SaveGameState(true);
        }
        else // we are in foreground again.
        {
            Debug.Log("unpaused");
            MainGameManager.Instance.SavedGameId = "";
            AdMobAds.Instance.ShowInterstatial();
        }
    }

    void OnApplicationQuit()
    {
        Debug.Log("Application quit. Saving game");
        SaveGameState(true);
    }
}

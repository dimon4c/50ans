﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class JsonFormatDb : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var allQuestions = JObject.Parse(MainGameManager.Instance.LoadQuestions("en"));
		for (int questIndex = 0; questIndex < allQuestions.AsArray.Count; questIndex++) 
		{
			/*
			if (allQuestions [gameIndex] ["right_answer"].ToString() == allQuestions [gameIndex] ["answer1"].ToString())
			{
				allQuestions [gameIndex] ["right_answer"].AsInt = 1;
			} else if (allQuestions [gameIndex] ["right_answer"].ToString() == allQuestions [gameIndex] ["answer2"].ToString())
			{
				allQuestions [gameIndex] ["right_answer"].AsInt = 2;
			} else if (allQuestions [gameIndex] ["right_answer"].ToString() == allQuestions [gameIndex] ["answer3"].ToString())
			{
				allQuestions [gameIndex] ["right_answer"].AsInt = 3;
			} else if (allQuestions [gameIndex] ["right_answer"].ToString() == allQuestions [gameIndex] ["answer4"].ToString())
			{
				allQuestions [gameIndex] ["right_answer"].AsInt = 4;
			}
			*/
			allQuestions[questIndex]["questionId"].AsInt = questIndex;
			//allQuestions [gameIndex].Add("questionId", gameIndex);
			allQuestions[questIndex]["id"].AsInt = questIndex;
		}
		Debug.Log (allQuestions.ToString());
	}
}

﻿[System.Serializable]
public class QuestionFromJson
{
    public int id;
    public string question;
    public string answer;
    public int difficulty;
    public int category;
}

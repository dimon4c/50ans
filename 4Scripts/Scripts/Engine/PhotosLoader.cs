﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhotosLoader : MonoBehaviour {
    private static PhotosLoader instance;
    public Dictionary<string, Texture2D> photosDictionary;

	void Awake () {
        if(instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(this); // or gameObject
        photosDictionary = new Dictionary<string, Texture2D>();
	}

    // Static singleton property
    public static PhotosLoader Instance
    {
        // Here we use the ?? operator, to return 'instance' if 'instance' does not equal null
        // otherwise we assign instance to a new component and return that
        get { return instance ?? (instance = new GameObject("PhotosLoader").AddComponent<PhotosLoader>()); }
    }

    public void AddPhotoByUrl (string photoUrl)
    {
        StartCoroutine(LoadUserPhoto(photoUrl));
    }

    IEnumerator LoadUserPhoto(string photoURL)
    {
//        if (photoURL == " ")
//            yield break;

        WWW www = new WWW(photoURL);
        yield return www;
        if (!photosDictionary.ContainsKey(photoURL))
            photosDictionary.Add(photoURL, www.texture);
    }

    public Texture2D GetUserPhoto (string photoUrl)
    {
//        if (photosDictionary.ContainsKey(photoUrl))
//        {
            Texture2D photoTexture = Texture2D.whiteTexture;
            photosDictionary.TryGetValue(photoUrl, out photoTexture);
            return photoTexture;
//        }
//        return null;
    }
}

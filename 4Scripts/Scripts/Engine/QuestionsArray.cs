﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class QuestionsArray
{
    //    public Game[] gamesArray;
    public List<QuestionFromJson> questionsArray;
}

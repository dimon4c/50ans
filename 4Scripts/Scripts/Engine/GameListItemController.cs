﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameListItemController : MonoBehaviour {
    public GameObject rematchButton;
    public GameObject playButton;
	public Text opponentName;
	public Text roundNoText;
	public Text lastMoveText;
	public Text winningText;
    public Image playerPhoto;
    public Image photoOutline;
	public Image curGameScreen;
	public FieldPreview fieldScript;
	public bool canPlay;
	public string gameId;

    public Sprite blueOutline;
    public Sprite redOutline;

    bool imageLoaded = false;
    public string photoUrl;

    public RectTransform viewPort;
    public Vector3 tempPos;

    public void SetOutline (bool redOne)
    {
        if (redOne)
            photoOutline.sprite = redOutline;
        else
            photoOutline.sprite = blueOutline;
        tempPos = this.GetComponent<RectTransform>().position;
        if (photoUrl != " ")
        {
            if (!PhotosLoader.Instance.photosDictionary.ContainsKey(photoUrl))
            {
                PhotosLoader.Instance.AddPhotoByUrl(photoUrl);
            }
            InvokeRepeating("LoadPhoto", 1f, 1f);
        }
    }

	public void PlayRound ()
	{
		if (canPlay) 
		{
			SessionHandler.Instance.currentSession = HomeScreen.getGameById (gameId);
			SceneManager.LoadScene ("GameField");
		}
	}

//    void OnBecameVisible()
//    {
//        Debug.Log("became visible");
//        if (!imageLoaded)
//            StartCoroutine(SetUserPhoto());
//    }

//    IEnumerator SetUserPhoto()
//    {
//        if (photoUrl == " ")
//            yield break;
//
//        WWW www = new WWW(photoUrl);
//        yield return www; 
//        imageLoaded = true;
//        Sprite sprite = new Sprite();
//        sprite = Sprite.Create (www.texture, new Rect (0,0,www.texture.width,www.texture.height), new Vector2(0.5f,0.5f));
//        playerPhoto.sprite = sprite;
//    }

    void LoadPhoto ()
    {
        if (PhotosLoader.Instance.photosDictionary.ContainsKey(photoUrl))
        {
            CancelInvoke("LoadPhoto");
            Texture2D photoTexture = PhotosLoader.Instance.GetUserPhoto(photoUrl);
            Sprite sprite = new Sprite();
            sprite = Sprite.Create(photoTexture, new Rect(0, 0, photoTexture.width, photoTexture.height), new Vector2(0.5f, 0.5f));
            playerPhoto.sprite = sprite;
        }
    }

    bool ShowsOnScreen ()
    {
        Vector3[] objectCorners = new Vector3[4];
        this.GetComponent<RectTransform>().GetWorldCorners(objectCorners);

        foreach (Vector3 corner in objectCorners)
        {
            if (!viewPort.rect.Contains(corner))
            {
                return false;
            }
        }
        return true;
    }

//    bool AboveViewPort ()
//    {
//        Vector3[] objectCorners = new Vector3[4];
//        this.GetComponent<RectTransform>().GetWorldCorners(objectCorners);
//        if (objectCorners[0].y > viewPort.rect.y)
//            return true;
//        return false;
//    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DrawPaths : MonoBehaviour {
	public GamePlay playScript;
	public GameObject linePrefab;
	public Transform parentForLines;
	Color redLineColor = new Color(204 /255.0f, 81 /255.0f, 46 /255.0f);
	Color blueLineColor = new Color(70 /255.0f, 171 /255.0f, 200 /255.0f);

	void Start ()
	{
		DrawPath ();
	}

	public void DrawPath () 
	{
		//remove childs
		int childCount = parentForLines.childCount;
		for (int i = 0; i < childCount; i++) {
			GameObject.Destroy(parentForLines.GetChild(i).gameObject);
		}

		for (int indexOfArray = 0; indexOfArray < playScript.cellValues.Length; indexOfArray++) 
		{
			int cellValue = playScript.cellValues [indexOfArray];
			if (cellValue == 1 || cellValue == 2) 
			{
				int xCoord = indexOfArray % 5;
				int yCoord = indexOfArray / 5;
				List <int[]> neighboursOfCell = GetCellNeighbours (xCoord , yCoord);
				for (int neighbourInd = 0; neighbourInd < neighboursOfCell.Count; neighbourInd++) 
				{
					int cellValuesIndex = neighboursOfCell [neighbourInd] [0] + neighboursOfCell [neighbourInd] [1] * 5;
					if (playScript.cellValues [cellValuesIndex] == cellValue) 
					{
						SpawnLine (xCoord, yCoord, neighboursOfCell [neighbourInd] [0], neighboursOfCell [neighbourInd] [1], cellValue);
					}
				}
			}
		}
	}

	List<int[]> GetCellNeighbours (int xCoord, int yCoord)
	{
		List <int[]> neighbours = new List<int[]> ();
		if (yCoord > 0) {
			neighbours.Add (new int[]{ xCoord, yCoord - 1 });
			if (xCoord < 4 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord + 1, yCoord - 1});
			if (xCoord > 0 && xCoord%2 == 0)
				neighbours.Add (new int[]{ xCoord - 1, yCoord - 1});
		}
		if (yCoord < 3) {
			neighbours.Add (new int[]{ xCoord, yCoord + 1 });
			if (xCoord%2 == 1 && xCoord < 4)
				neighbours.Add (new int[]{ xCoord + 1, yCoord + 1});
			if (xCoord%2 == 1 && xCoord > 0)
				neighbours.Add (new int[]{ xCoord - 1, yCoord + 1});
		}
		if (xCoord > 0)
			neighbours.Add (new int[]{ xCoord - 1, yCoord});
		if (xCoord < 4)
			neighbours.Add (new int[]{ xCoord + 1, yCoord});
		return neighbours;
	}

	void SpawnLine (int firstCellX, int firstCellY, int secondCellX, int secondCellY, int value)
	{
		RectTransform firstCellRect = null;
		RectTransform secondCellRect = null;
		foreach (Cell cell in playScript.cells) 
		{
			if (cell.cellX == firstCellX && cell.cellY == firstCellY)
				firstCellRect = cell.gameObject.GetComponent<RectTransform> ();
			else if (cell.cellX == secondCellX && cell.cellY == secondCellY)
				secondCellRect = cell.gameObject.GetComponent<RectTransform> ();
		}
		//float lineAngle = Vector2.Angle (firstCellRect.anchoredPosition, secondCellRect.anchoredPosition);
		float lineAngle = GetAngle(firstCellRect.anchoredPosition.x,firstCellRect.anchoredPosition.y, secondCellRect.anchoredPosition.x, secondCellRect.anchoredPosition.y);
		GameObject newLine = (GameObject)  Instantiate (linePrefab, Vector3.zero, Quaternion.identity);
		newLine.transform.SetParent (parentForLines); 
		newLine.transform.localScale = Vector3.one;
		newLine.GetComponent<RectTransform>().sizeDelta = new Vector2 (10, 100);
		newLine.transform.localRotation = Quaternion.Euler(new Vector3 (0, 0, lineAngle));
		newLine.GetComponent<RectTransform> ().anchoredPosition = Vector2.Lerp (firstCellRect.anchoredPosition, secondCellRect.anchoredPosition, .5f);
		newLine.GetComponent<Image> ().color = redLineColor;
		if (value == 2)
			newLine.GetComponent<Image> ().color = blueLineColor;
		newLine.name = "line " + firstCellX.ToString () + firstCellY.ToString () + " " + secondCellX.ToString () + secondCellY.ToString ();
	}

	float GetAngle(float X1, float Y1, float X2, float Y2) {
		if (Y2 == Y1)
			return 90;
		if (X2 == X1)
			return 0;

		float tangent = (X2 - X1) / (Y2 - Y1);
		double ang = (float) Mathf.Atan(tangent) * 57.2958;
		if (Y2-Y1 < 0) ang -= 180;
		ang *= -1;
		return (float) ang;
	}
}

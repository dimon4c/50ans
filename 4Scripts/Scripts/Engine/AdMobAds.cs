﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdMobAds : MonoBehaviour {
	private static AdMobAds _instance;

	public static AdMobAds Instance
	{
		get{
			if(_instance == null)
			{
				GameObject go = new GameObject ("AdMobAds");
				go.AddComponent<AdMobAds> ();
				DontDestroyOnLoad(go);
			}
			return _instance;}}

	void Awake()
	{
		_instance = this;
		RequestInterstitial ();
		//RequestBanner ();
	}

	InterstitialAd interstitial;
	BannerView bannerView;

	public void ShowInterstatial ()
	{
		Debug.Log ("Interstatial check for load");
		if (interstitial.IsLoaded ()) 
		{
			Debug.Log ("Interstatial loaded");
			interstitial.Show ();
		}
	}

	public void ShowBanner ()
	{
		bannerView.Show ();
	}

	public void HideBanner ()
	{
		bannerView.Hide ();
	}

	public void DestroyAd ()
	{
		bannerView.Destroy();
		interstitial.Destroy();
	}

	private void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-7810685228980169/3940751935";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7810685228980169/8370951534";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}



	private void RequestInterstitial()
	{
		#if UNITY_ANDROID
        //string adUnitId = "ca-app-pub-7810685228980169/5417485130"; //real check
        string adUnitId = "ca-app-pub-3940256099942544/1033173712"; //test
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7810685228980169/9847684732";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
		// Create an empty ad request.
		//AdRequest request = new AdRequest.Builder().Build();
		AdRequest request = new AdRequest.Builder()
		.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
		.AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
		.Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}
	
	public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		Debug.Log("Ad Failed to load: " + args.Message);
		// Handle the ad failed to load event.
	}
}

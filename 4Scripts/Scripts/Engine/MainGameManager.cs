﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

//using SimpleJSON;

public class MainGameManager : MonoBehaviour
{
	
    private static MainGameManager _instance;

    public static MainGameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("MainGameManager");
                go.AddComponent<MainGameManager>();
                DontDestroyOnLoad(go);
            }
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    //Save data in a txt file
    public void SaveGames(String gamesJSON, Game gameToUpdate)
    {
//        Debug.Log("gamesToSave: " + gamesJSON);
        if (gamesJSON == "")
        {
            gamesJSON = "{\"gamesArray\":\" \"}";
            Debug.Log("creating new file: " + gamesJSON);
        }
//        string JSONToParse = "{\"gamesArray\":" + gamesJSON + "}";
//        Debug.Log("JSONToParse: " + JSONToParse);
        GamesArray allGames = JsonUtility.FromJson<GamesArray>(gamesJSON);
       
        if (gameToUpdate != null)
        {
            bool gameFound = false;
            //int foundGameIndex = 0;
            for (int gameIndex = 0; gameIndex < allGames.gamesArray.Count; gameIndex++)
            {
                if (allGames.gamesArray[gameIndex].gameId == gameToUpdate.gameId)
                {
                    Debug.Log("game found with id: " + allGames.gamesArray[gameIndex].gameId);
                    gameFound = true;
                    //foundGameIndex = gameIndex;
                    allGames.gamesArray[gameIndex] = gameToUpdate;
                }
            }
            if (!gameFound)
                allGames.gamesArray.Add(gameToUpdate);
        }
        string newJson = JsonUtility.ToJson(allGames);
//        Debug.Log("newJson: " + newJson);

        String fileLocation = Application.persistentDataPath + "/games.json";
        //check if file is exist and delete them
        if (File.Exists(fileLocation))
        {
            File.Delete(fileLocation);
        } 

        StreamWriter file = File.CreateText(fileLocation);
        file.WriteLine(newJson);
        file.Close();
//		Debug.Log ("game saved");
    }

    //Load data from a txt file
    public String LoadGames()
    {
        String fileLocation = Application.persistentDataPath + "/games.json";
//		Debug.Log (fileLocation);
        if (File.Exists(fileLocation))
        {
            StreamReader file = new StreamReader(fileLocation);

            while (!file.EndOfStream)
            {
                String gamesJSON = file.ReadLine();
                file.Close();
                return gamesJSON;
            }	
        }
        else
        {
            Debug.Log("Can't find file Load Games");
            //loading games from assets
            //for local testing purposes only
            //TextAsset questFile = Resources.Load("JSONTest") as TextAsset;
            //return questFile.text;
        }
        return "";
    }

    public void RemoveGames()
    {
        String fileLocation = Application.persistentDataPath + "/games.json";
        //check if file is exist and delete them
        if (File.Exists(fileLocation))
        {
            Debug.Log("removing games file from documents");
            File.Delete(fileLocation);
        } 
    }

    //Save a question in format question + "lang"
    public void SaveQuestions(String question, String lang)
    {
        String fileLocation = Application.persistentDataPath + "/Questions_" + MainGameManager.Instance.LocalLanguage + ".json";
        //check if file is exist and delete them
        if (File.Exists(fileLocation))
        {
            File.Delete(fileLocation);
        } 

        StreamWriter file = File.CreateText(fileLocation); //new StreamWriter (fileLocation, false);
        file.WriteLine(question);
        file.Close();
    }
    //Load a questions from a txt file
    public String LoadQuestions(String lang)
    {
        // Debug.Log(MainGameManager.Instance.LocalLanguage);
        String fileLocation = Application.persistentDataPath + "/Questions_" + MainGameManager.Instance.LocalLanguage + ".json";
        if (File.Exists(fileLocation))
        {
            StreamReader file = new StreamReader(fileLocation);

            while (!file.EndOfStream)
            {
                String questionStr = file.ReadLine();
                file.Close();
                Debug.Log("questionStr" + questionStr);
                return questionStr;
            }	
        }
        else
        {
            Debug.Log("Can't find file Load Questions");
            //loading file from assets resources
            TextAsset questFile = Resources.Load("Questions_" + MainGameManager.Instance.LocalLanguage) as TextAsset;
            //TextAsset questFile = Resources.Load("Questions_" + lang) as TextAsset;
            Debug.Log("questFile" + questFile.text);
            return questFile.text;
        }
        return "";
    }

    public void RemoveQuestions(String lang)
    {
        String fileLocation = Application.persistentDataPath + "/Questions_" + MainGameManager.Instance.LocalLanguage + ".json";
        //check if file is exist and delete them
        if (File.Exists(fileLocation))
        {
            File.Delete(fileLocation);
        } 
    }

    //Saved game id
    public string LocalLanguage { get { return PlayerPrefs.GetString("LocalLanguage", Application.systemLanguage.ToString()); } set { PlayerPrefs.SetString("LocalLanguage", value); } }

    //Saved game id
    public string SavedGameId { get { return PlayerPrefs.GetString("SavedGameId", ""); } set { PlayerPrefs.SetString("SavedGameId", value); } }

    // Time to answer
    public int TimeToAnswer { get { return 20; } }
    // Time to add when switch question
    public int TimeToAddSwitch { get { return PlayerPrefs.GetInt("TimeToAddSwitch", 10); } set { PlayerPrefs.SetInt("TimeToAddSwitch", value); } }
    // Time to add when use action
    public int TimeToAdd { get { return PlayerPrefs.GetInt("TimeToAdd", 30); } set { PlayerPrefs.SetInt("TimeToAdd", value); } }

    // Player Level
    public int LocalPlayerLevel { get { return PlayerPrefs.GetInt("PlayerLevel", 1); } set { PlayerPrefs.SetInt("PlayerLevel", value); } }
    // Player Name
    public string LocalPlayerName { get { return PlayerPrefs.GetString("PlayerName", "My Name"); } set { PlayerPrefs.SetString("PlayerName", value); } }
    // Player Photo
    public string LocalPlayerPhoto { get { return PlayerPrefs.GetString("PlayerPhoto", " "); } set { PlayerPrefs.SetString("PlayerPhoto", value); } }
    // Player Id
    public string LocalPlayerId { get { return PlayerPrefs.GetString("PlayerId", "23131213213213"); } set { PlayerPrefs.SetString("PlayerId", value); } }
    // Player Password
    public string LocalPlayerPass { get { return PlayerPrefs.GetString("PlayerPass", "23131213213213"); } set { PlayerPrefs.SetString("PlayerPass", value); } }

    //Has registered user
    public bool HasPlayer { get { return PlayerPrefs.HasKey("PlayerId"); } }
    // Total Points Earned
    public int TotalScore{ get { return PlayerPrefs.GetInt("TotalScore", 0); } set { PlayerPrefs.SetInt("TotalScore", value); } }
    // Points Earned Weekly
    public int WeeklyScore{ get { return PlayerPrefs.GetInt("WeeklyScore", 0); } set { PlayerPrefs.SetInt("WeeklyScore", value); } }
    // Best Answer Streak
    public int AnswerStreak { get { return PlayerPrefs.GetInt("AnswerStreak", 0); } set { PlayerPrefs.SetInt("AnswerStreak", value); } }
    // Played Games
    public int TotalGames { get { return PlayerPrefs.GetInt("TotalPlayed", 0); } set { PlayerPrefs.SetInt("TotalPlayed", value); } }
    // Wins game
    public int GameWins { get { return PlayerPrefs.GetInt("Wins", 0); } set { PlayerPrefs.SetInt("Wins", value); } }
    // Loses games
    public int LosesGame { get { return PlayerPrefs.GetInt("Loses", 0); } set { PlayerPrefs.SetInt("Loses", value); } }
    // Correct Answers
    public int CorrectAnswers { get { return PlayerPrefs.GetInt("CorrectAnswers", 0); } set { PlayerPrefs.SetInt("CorrectAnswers", value); } }
    // Wrong Answers
    public int WrongAnswers { get { return PlayerPrefs.GetInt("WrongAnswers", 0); } set { PlayerPrefs.SetInt("WrongAnswers", value); } }

    // Correct Answers level 1
    public int CorrectAnswersLevel1 { get { return PlayerPrefs.GetInt("CorrectAnswersLevel1", 0); } set { PlayerPrefs.SetInt("CorrectAnswersLevel1", value); } }
    // Wrong Answers level 1
    public int WrongAnswersLevel1 { get { return PlayerPrefs.GetInt("WrongAnswersLevel1", 0); } set { PlayerPrefs.SetInt("WrongAnswersLevel1", value); } }
    // Correct Answers level 2
    public int CorrectAnswersLevel2 { get { return PlayerPrefs.GetInt("CorrectAnswersLevel2", 0); } set { PlayerPrefs.SetInt("CorrectAnswersLevel2", value); } }
    // Wrong Answers level 2
    public int WrongAnswersLevel2 { get { return PlayerPrefs.GetInt("WrongAnswersLevel2", 0); } set { PlayerPrefs.SetInt("WrongAnswersLevel2", value); } }
    // Correct Answers level 3
    public int CorrectAnswersLevel3 { get { return PlayerPrefs.GetInt("CorrectAnswersLevel3", 0); } set { PlayerPrefs.SetInt("CorrectAnswersLevel3", value); } }
    // Wrong Answers level 3
    public int WrongAnswersLevel3 { get { return PlayerPrefs.GetInt("WrongAnswersLevel3", 0); } set { PlayerPrefs.SetInt("WrongAnswersLevel3", value); } }
    // Correct Answers level 4
    public int CorrectAnswersLevel4 { get { return PlayerPrefs.GetInt("CorrectAnswersLevel4", 0); } set { PlayerPrefs.SetInt("CorrectAnswersLevel4", value); } }
    // Wrong Answers level 4
    public int WrongAnswersLevel4 { get { return PlayerPrefs.GetInt("WrongAnswersLevel4", 0); } set { PlayerPrefs.SetInt("WrongAnswersLevel4", value); } }

    // Player best letter
    public string BestLetter { get { return PlayerPrefs.GetString("BestLetter", " "); } set { PlayerPrefs.SetString("BestLetter", value); } }
    // Player worse letter
    public string WorseLetter { get { return PlayerPrefs.GetString("WorseLetter", " "); } set { PlayerPrefs.SetString("WorseLetter", value); } }
    // Amount of money
    public int PlayerMoney { get { return PlayerPrefs.GetInt("Money", 0); } set { PlayerPrefs.SetInt("Money", value); } }
    // Amount of coins
    public int PlayerCoins { get { return PlayerPrefs.GetInt("Coins", 0); } set { PlayerPrefs.SetInt("Coins", value); } }

    //For Rematch
    public string OpponentId { get { return PlayerPrefs.GetString("OpponentId", ""); } set { PlayerPrefs.SetString("OpponentId", value); } }

    public string OpponentName { get { return PlayerPrefs.GetString("OpponentName"); } set { PlayerPrefs.SetString("OpponentName", value); } }

    public string OpponentPhotoLink { get { return PlayerPrefs.GetString("OpponentPhotoLink"); } set { PlayerPrefs.SetString("OpponentPhotoLink", value); } }
}
﻿using UnityEngine;
using System.Collections;
using System;

//This allows the IComparable Interface

//This is the class you will be storing in the different collections. In order to use
//a collection's Sort() method, this class needs to implement the IComparable interface.
[Serializable]
public class Game : IComparable<Game>
{
    public string gameId;

    public string gameStarted;
    public string lastTurn;

    public bool finished;
    public bool redWins;
    public string turn;

    public bool isThereAnswerCell;
    public int cellToAnswerX;
    public int cellToAnswerY;

    public string bluePlayerName;
    public string bluePlayerPhotoLink;
    public string bluePlayerId;
    public int blueNoOfFreeShuffle;
    public int blueNoOfFreeChallenge;
    public int bluePlayerAnswersNo;
    public int bluePlayerRightAnswersNo;
    public string bluePlayerBestLetter;
    public string bluePlayerWorseLetter;

    public string redPlayerName;
    public string redPlayerPhotoLink;
    public string redPlayerId;
    public int redNoOfFreeShuffle;
    public int redNoOfFreeChallenge;
    public int redPlayerAnswersNo;
    public int redPlayerRightAnswersNo;
    public string redPlayerBestLetter;
    public string redPlayerWorseLetter;

    public int roundNo;

    public int[] cellValues;
    public string[] cellCategories;


    //This method is required by the IComparable interface.
    public int CompareTo(Game other)
    {
        if (other == null)
        {
            return 1;
        }

        if (lastTurn == "" || lastTurn == null)
        {
            //return 1;
            lastTurn = System.DateTime.UtcNow.ToString();
        }
        System.DateTime thisDate = System.DateTime.Parse(lastTurn);
        //Debug.Log ("gameId: " + other.gameId);
        //Debug.Log ("parsing:" + other.lastTurn + "|");
        if (other.lastTurn == "" || other.lastTurn == null)
        {
            //return 1;
            other.lastTurn = System.DateTime.UtcNow.ToString();
        }
        System.DateTime otherDate = System.DateTime.Parse(other.lastTurn);
        return System.DateTime.Compare(otherDate, thisDate);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.storage;

public class MultiplayerGames : MonoBehaviour {
    Constant cons = new Constant ();
    ServiceAPI sp = null;
    StorageService storageService = null; // Initialising Storage Service.
    GamesResponse callBackStorage = new GamesResponse ();

    void Awake () 
    {
        sp = new ServiceAPI (cons.apiKey, cons.secretKey);

        // Enabling offline storage.  
        App42API.SetOfflineStorage (true);   

    }

    public void GetPlayerGames ()
    {
        storageService = sp.BuildStorageService (); // Initializing Storage Service.
        // Build query q1 for name equal to Nick      
        Query q1 = QueryBuilder.Build("redPlayerId", MainGameManager.Instance.LocalPlayerId, Operator.EQUALS);   
        // Build query q2 for age greater than 30     
        Query q2 = QueryBuilder.Build("bluePlayerId", MainGameManager.Instance.LocalPlayerId, Operator.EQUALS);      
        // Apply AND between q1 and q2    
        Query playerGamesQuery = QueryBuilder.CompoundOperator(q1, Operator.OR, q2);   
        // Pass aggregated query q3 to the finder method below. Similarly you can aggregate more conditions while querying the object.    
        storageService.FindDocumentsByQuery(cons.dbName, cons.gamesCollectionName, playerGamesQuery, callBackStorage);
    }

    public void UpdateGamesById (string oldId)
    {
        StartCoroutine(UpdatingProcess(oldId));

    }

    IEnumerator UpdatingProcess (string oldUSerId)
    {
        callBackStorage.Reset();

        storageService = sp.BuildStorageService ();
        Query q1 = QueryBuilder.Build("redPlayerId", oldUSerId, Operator.EQUALS);   
        Query q2 = QueryBuilder.Build("bluePlayerId", oldUSerId, Operator.EQUALS);      
        Query playerGamesQuery = QueryBuilder.CompoundOperator(q1, Operator.OR, q2);   
        storageService.FindDocumentsByQuery(cons.dbName, cons.gamesCollectionName, playerGamesQuery, callBackStorage);

        yield return new WaitUntil (() => callBackStorage.successed || callBackStorage.exceptioned);

        if (callBackStorage.successed)
        {
            foreach (string gameStr in callBackStorage.gamesList)
            {
                Game playerGame = JsonUtility.FromJson<Game>(gameStr);
                if (playerGame.bluePlayerId == oldUSerId)
                {
                    playerGame.bluePlayerId = MainGameManager.Instance.LocalPlayerId;
                    playerGame.bluePlayerName = MainGameManager.Instance.LocalPlayerName;
                    playerGame.bluePlayerPhotoLink = MainGameManager.Instance.LocalPlayerPhoto;
                } else
                {

                    playerGame.redPlayerId = MainGameManager.Instance.LocalPlayerId;
                    playerGame.redPlayerName = MainGameManager.Instance.LocalPlayerName;
                    playerGame.redPlayerPhotoLink = MainGameManager.Instance.LocalPlayerPhoto;
                }
                storageService.UpdateDocumentByKeyValue(cons.dbName, cons.gamesCollectionName, "gameId", playerGame.gameId, JsonUtility.ToJson(playerGame), callBackStorage);
            }
        }
    }
}

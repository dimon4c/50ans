﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
//using SimpleJSON;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.user;
using com.shephertz.app42.paas.sdk.csharp.storage;
using com.shephertz.app42.paas.sdk.csharp.pushNotification;

using AssemblyCSharpfirstpass;
using System.Runtime.InteropServices;

public class PushNotificationsScript : MonoBehaviour, App42NativePushListener {
    public GameObject pushWindowPrefab;

    Constant cons = new Constant ();

    [System.Runtime.InteropServices.DllImport ("__Internal")]
    extern static public void registerForRemoteNotifications ();

    [System.Runtime.InteropServices.DllImport ("__Internal")]
    extern static public void setListenerGameObject (string listenerName);

    PushNotificationService pushNotificationService = null;          // Initializing PushNotification Service.
    PushNotificationResponse pushCallBack = new PushNotificationResponse();// Making callBack Object for PushNotificationResponse.
    public string deviceToken;
    ServiceAPI sp = null;

    private static PushNotificationsScript _instance;

    public static PushNotificationsScript Instance
    {
        get{
            return _instance;
        }
    }
 
	// Use this for initialization
    void Awake () {
        _instance = this;
        DontDestroyOnLoad(gameObject);

//        sp = new ServiceAPI (cons.apiKey, cons.secretKey);
//        App42API.SetLoggedInUser(MainGameManager.Instance.LocalPlayerId);
//
//        App42Push.setApp42PushListener (this);
//        Debug.Log("HERE GOES ANDROID !");
//        #if UNITY_ANDROID
//        string googleProjectNo = "82505982659";
//        App42Push.registerForPush (googleProjectNo);
//        Debug.Log("Last message: " + App42Push.getLastPushMessage()); 
//        #endif 
//        //for iOS??!!
//        #if UNITY_IOS
//            setListenerGameObject (this.gameObject.name);
//        #endif

        //App42Log.SetDebug(true);
        App42Push.setApp42PushListener (this);
        sp = new ServiceAPI (cons.apiKey, cons.secretKey);
        App42API.Initialize (cons.apiKey, cons.secretKey);
        App42API.SetLoggedInUser (MainGameManager.Instance.LocalPlayerId);
        #if UNITY_IOS
        #if !UNITY_EDITOR
        setListenerGameObject (this.gameObject.name);
        registerForRemoteNotifications ();
        #endif
        #endif


        #if UNITY_ANDROID
        App42Push.registerForPush ("82505982659"); //roi
        //App42Push.registerForPush ("316666677625"); //my
        Debug.Log("Last message: " + App42Push.getLastPushMessage()); 
        #endif 
        //#if !UNITY_EDITOR
        //App42API.SetLoggedInUser (MainGameManager.Instance.LocalPlayerId);
        //#endif
	}

    public void ShowNotification (string message)
    {
        GameObject newNotification = (GameObject)  Instantiate (pushWindowPrefab, Vector3.zero, Quaternion.identity) ;
        //letterSlot [i] = clone;
        Transform canvasTrans = GameObject.Find("Canvas").transform;
        newNotification.transform.SetParent (canvasTrans); 
        newNotification.transform.localScale = Vector3.one;
        newNotification.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 50);
        newNotification.GetComponent<PushWindow>().notificationMessage.text = message;
    }
	
    public void onDeviceToken(String deviceToken)
    {
        Debug.Log("ON DEVICE TOKEN!!!!");
        string message="Device token from native: "+deviceToken;
        string deviceType = App42Push.getDeviceType ();
        Debug.Log ("StoreDeviceToken: " + deviceToken);

        if (deviceType != null && deviceToken != null && deviceToken.Length != 0) 
        {
            pushNotificationService = sp.BuildPushNotificationService (); // Initializing PushNotification Service.
            pushNotificationService.StoreDeviceToken (App42API.GetLoggedInUser(), deviceToken, deviceType, pushCallBack);
        }
        Debug.Log (message);
    }

    public void onMessage(String msg)
    {
        string message="Message From native: "+msg;
        Debug.Log (message);
        ShowNotification(msg);
        MultiplayerGames gamesLoadScript = Camera.main.GetComponent<MultiplayerGames>();
        if (gamesLoadScript)
            gamesLoadScript.GetPlayerGames();
    }
    public void onError(String error){
        string message="Error From native: "+error;
        Debug.Log (message);
    }

    public void CreateChannelForApp () 
    {
        pushNotificationService = sp.BuildPushNotificationService (); // Initializing PushNotification Service.
        pushNotificationService.CreateChannelForApp ("TestChannel", "Test channel to delete", pushCallBack);
    }

    public void StoreDeviceToken () 
    {
        Debug.Log ("StoreDeviceToken: " + deviceToken);
        String deviceType = App42Push.getDeviceType ();
        pushNotificationService = sp.BuildPushNotificationService (); // Initializing PushNotification Service.
        pushNotificationService.StoreDeviceToken (MainGameManager.Instance.LocalPlayerId, deviceToken, deviceType, pushCallBack);
    }

    public void SendPushMessageToUser (string userToSend, string messageToSend) 
    {
        Debug.Log ("SendPushMessageToUser: " + userToSend + " message: " + messageToSend);

        Dictionary<String, String> otherMetaHeaders = new Dictionary<String, String>();
        otherMetaHeaders.Add("myKey", "true");

        pushNotificationService = sp.BuildPushNotificationService (); // Initializing PushNotification Service.
        pushNotificationService.SetOtherMetaHeaders(otherMetaHeaders);
        pushNotificationService.SendPushMessageToUser (userToSend, messageToSend, pushCallBack);

    }
}

﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;

using UnityEngine.UI;
public class FacebookInit : MonoBehaviour {
    public Image playerPhotoImage;
    void Awake ()
    {
        if (!FB.IsInitialized) 
        {
            // Initialize the Facebook SDK
            FB.Init(OnInitComplete, OnHideUnity);
        } else 
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

	void Start () 
    {
        if (FB.IsLoggedIn) {
            FetchFBProfile();
        }
        if (MainGameManager.Instance.LocalPlayerPhoto != " ")
            StartCoroutine(SetUserPhoto());
	}

    public void ConnectToFacebook ()
    {
        if (!FB.IsLoggedIn && FB.IsInitialized)
            CallFBLogin();
    }

    void CheckUserJSON (string userId, string userName, string photoLink)
    {
        if (MainGameManager.Instance.LocalPlayerId != userId)
        {
            string oldPlayerId = MainGameManager.Instance.LocalPlayerId;
            //save to prefs
            //where to store old id to update game list and user json?
            MainGameManager.Instance.LocalPlayerId = userId;
            MainGameManager.Instance.LocalPlayerName = userName;
            MainGameManager.Instance.LocalPlayerPhoto = photoLink;

            gameObject.GetComponent<HomeScreen>().PopulateUI();
            gameObject.GetComponent<MultiplayerUser>().CreateUserDoc(true, oldPlayerId);
            gameObject.GetComponent<MultiplayerGames>().UpdateGamesById(oldPlayerId);
        }
    }

    #region FB.Init() methods
    private void OnInitComplete()
    {
        Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);

        if (FB.IsInitialized) {

            if (FB.IsLoggedIn) {
                FetchFBProfile();
                StartCoroutine(SetUserPhoto());
            }
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK

        } else {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
//        if (Application.internetReachability == NetworkReachability.NotReachable)
//        {
//            // no internet !!!!
//        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log("Is game showing? " + isGameShown);
    }
    #endregion

    #region FB.Login() example

    private void CallFBLogin()
    {
        var perms = new List<string>(){"public_profile", "email", "user_friends"};
        FB.LogInWithReadPermissions(perms, LoginCallback);
    }

    void LoginCallback(ILoginResult result)
    {
        if (result.Error != null)
            Debug.Log ("Error Response:\n" + result.Error);
        else if (!FB.IsLoggedIn)
        {
            Debug.Log ( "Login cancelled by Player");
        }
        else
        {
            Debug.Log ("Login was successful!");
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);

            FetchFBProfile();
            StartCoroutine(SetUserPhoto());
        }
    }

    private void CallFBLogout()
    {
        FB.LogOut();
    }
    #endregion

    IEnumerator SetUserPhoto()
    {
        Debug.Log("loading photo from:" + GetUserAvatar());
        string path = GetUserAvatar();
        WWW www = new WWW(path);
        yield return www; 
        Debug.Log("ASSIGNING PHOTO!");
        Sprite sprite = new Sprite();
        sprite = Sprite.Create (www.texture, new Rect (0,0,www.texture.width,www.texture.height), new Vector2(0.5f,0.5f));
        playerPhotoImage.sprite = sprite;
    }

    private void FetchFBProfile () 
    {
        FB.API("/me?fields=first_name,last_name,email", HttpMethod.GET, FetchProfileCallback, new Dictionary<string,string>(){});
    }

    private void FetchProfileCallback (IGraphResult result) 
    {
        Debug.Log (result.RawResult);
        Dictionary<string,object> FBUserDetails = (Dictionary<string,object>)result.ResultDictionary;
        Debug.Log ("Profile: first name: " + FBUserDetails["first_name"]);
        Debug.Log ("Profile: last name: " + FBUserDetails["last_name"]);
        CheckUserJSON(AccessToken.CurrentAccessToken.UserId, FBUserDetails["first_name"] + " " + FBUserDetails["last_name"], GetUserAvatar());
    }

    string GetUserAvatar ()
    {
        if (MainGameManager.Instance.LocalPlayerPhoto != " ")
            return MainGameManager.Instance.LocalPlayerPhoto;
        
        return "http://graph.facebook.com/" + AccessToken.CurrentAccessToken.UserId + "/picture?width=128&height=128";
    }

    string GetUserId ()
    {
        return AccessToken.CurrentAccessToken.UserId;
    }
}

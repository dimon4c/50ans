﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//using SimpleJSON;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.storage;

public class HomeScreen : MonoBehaviour
{
    static List<Game> yourTurnGames;
    public List<Game> opponentTurnGames;
    public List<Game> finishedGames;

    public GameObject yourTurnGamesTitle;
    public GameObject oppTurnGamesTitle;
    public GameObject finishedGamesTitle;
    public GameObject activeGameList;

    public Transform contentTrans;

    public Text userNameText;
    public Text gamesPlayedText;
    public Text pointsEarnedText;
    public Text victoriesText;
    public Text answerStreakText;
    public Text rankText;
    public Text coinsText;

    public MultiplayerUser userService;
    public MultiplayerGames gamesService;

    public RectTransform viewPort;

    public RectTransform statsRect;

    public AILogic aiScript;

    // Use this for initialization
    void Start()
    {
        //for testing purposes
        //MainGameManager.Instance.PlayerCoins = 5;
        MainGameManager.Instance.RemoveQuestions("en");
        //MainGameManager.Instance.RemoveGames ();
        //PlayerPrefs.DeleteAll();

        Debug.Log("Language: " + MainGameManager.Instance.LocalLanguage);
        if (MainGameManager.Instance.LocalLanguage != "English" && MainGameManager.Instance.LocalLanguage != "Hebrew")
        {    
            MainGameManager.Instance.LocalLanguage = "English";
            //TODO: notify localize script and UI to change
        }
            

        CheckForLevel();
        PopulateUI();
        LoadGames(); //loading games from temp file for faster UI
       
        if (MainGameManager.Instance.SavedGameId != "") //load saved game
        { 
            SessionHandler.Instance.currentSession = getGameById(MainGameManager.Instance.SavedGameId);
            SceneManager.LoadScene("GameField");
        }
//        else
//        {
//            if (PlayerPrefs.GetInt("gameToUpdate") != 0)
//            {
//                Invoke("CheckForUpdateTheGame", 0.01f);
//            }
//            else
//            {
//                gamesService.GetPlayerGames(); //loading games from storage App42
//            }
//        }

    }

    void CheckForLevel()
    {
        int numberOfRightAnswersLevel1 = MainGameManager.Instance.CorrectAnswersLevel1;
        int numberOfAnswersLevel1 = numberOfRightAnswersLevel1 + MainGameManager.Instance.WrongAnswersLevel1;
        if (numberOfAnswersLevel1 > 5)
        {
            float percentOfRightAnswers1 = numberOfRightAnswersLevel1 / numberOfAnswersLevel1 * 100F;
            if (percentOfRightAnswers1 >= 75)
                MainGameManager.Instance.LocalPlayerLevel = 2;
        }
        if (MainGameManager.Instance.LocalPlayerLevel == 2)
        {
            int numberOfRightAnswersLevel2 = MainGameManager.Instance.CorrectAnswersLevel2;
            int numberOfAnswersLevel2 = numberOfRightAnswersLevel2 + MainGameManager.Instance.WrongAnswersLevel2;
            if (numberOfAnswersLevel2 > 5)
            {
                float percentOfRightAnswers2 = numberOfRightAnswersLevel2 / numberOfAnswersLevel2 * 100F;
                if (percentOfRightAnswers2 >= 75)
                    MainGameManager.Instance.LocalPlayerLevel = 3;
            }
        }
        if (MainGameManager.Instance.LocalPlayerLevel == 3)
        {
            int numberOfRightAnswersLevel3 = MainGameManager.Instance.CorrectAnswersLevel3;
            int numberOfAnswersLevel3 = numberOfRightAnswersLevel3 + MainGameManager.Instance.WrongAnswersLevel3;
            if (numberOfAnswersLevel3 > 5)
            {
                float percentOfRightAnswers3 = numberOfRightAnswersLevel3 / numberOfAnswersLevel3 * 100F;
                if (percentOfRightAnswers3 >= 75)
                    MainGameManager.Instance.LocalPlayerLevel = 4;
            }
        }
    }

    public void ReloadGames()
    {
        gamesService.GetPlayerGames(); //loading games from storage
    }

    void CheckForUpdateTheGame()
    {
        PlayerPrefs.SetInt("gameToUpdate", 0);
        //update the game
        if (SessionHandler.Instance.currentSession.bluePlayerId == " ") //new game find opponent
            userService.FindOppPlayer();
        else
        {
            if (SessionHandler.Instance.currentSession.bluePlayerId == MainGameManager.Instance.LocalPlayerId)
                userService.UploadGame(null, SessionHandler.Instance.currentSession.redPlayerId);
            else
                userService.UploadGame(null, SessionHandler.Instance.currentSession.bluePlayerId);
        }
    }

    public void LoadGamesFromStorage(List<string> gamesList)
    {
        yourTurnGames = new List<Game>();
        opponentTurnGames = new List<Game>();
        finishedGames = new List<Game>();

//        string[] gamesArrJ = new string[gamesList.Count];
//        int indexOfGamesArrJ = 0;
        string gamesArrJ = "[";
        foreach (string gameJson in gamesList)
        {
            Game game = JsonUtility.FromJson<Game>(gameJson);
//            Debug.Log("game: " + gameJson);
//            gamesArrJ[indexOfGamesArrJ] = JsonUtility.ToJson(game);
//            indexOfGamesArrJ++;
            gamesArrJ += gameJson + ",";
//            gamesArrJ.Add(game);

            if (game.finished)
            {
                finishedGames.Add(game);
            }
            else
            {
                bool localBlue = false;
                if (System.String.Equals(game.bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                    localBlue = true;
                if (localBlue)
                {
                    if (System.String.Equals(game.turn, "blue"))
                        yourTurnGames.Add(game);
                    else
                        opponentTurnGames.Add(game);
                }
                else
                {
                    if (System.String.Equals(game.turn, "red"))
                        yourTurnGames.Add(game);
                    else
                        opponentTurnGames.Add(game);
                }
            }
        }
//        gamesArrJ.Remove(gamesArrJ.Length - 1);
        gamesArrJ = gamesArrJ.Substring(0, gamesArrJ.Length - 1);
        gamesArrJ += "]";
        gamesArrJ = "{\"gamesArray\":" + gamesArrJ + "}";
        MainGameManager.Instance.SaveGames(gamesArrJ.ToString(), null);

        aiScript.AITurns();

        PopulateGamesScroll();
    }

    public void LoadGames()
    {
        yourTurnGames = new List<Game>();
        opponentTurnGames = new List<Game>();
        finishedGames = new List<Game>();
        if (MainGameManager.Instance.LoadGames() == "")
            return;
        
//        string JSONToParse = "{\"gamesArray\":" + MainGameManager.Instance.LoadGames() + "}";
        string JSONToParse = MainGameManager.Instance.LoadGames();

        GamesArray allGames = JsonUtility.FromJson<GamesArray>(JSONToParse);
//        Game[] allGames = JsonUtility.FromJson<Game[]>(MainGameManager.Instance.LoadGames());
//        var allGames = JObject.Parse(MainGameManager.Instance.LoadGames());
        if (allGames == null)
            return;
//        Debug.Log("allgames from file: " + allGames.gamesArray.ToString());
        for (int gameIndex = 0; gameIndex < allGames.gamesArray.Count; gameIndex++)
        {
            if (allGames.gamesArray[gameIndex].finished)
            {
                finishedGames.Add(allGames.gamesArray[gameIndex]);
            }
            else
            {
                bool localBlue = false;
                if (System.String.Equals(allGames.gamesArray[gameIndex].bluePlayerId, MainGameManager.Instance.LocalPlayerId))
                    localBlue = true;
                if (localBlue)
                {
                    if (System.String.Equals(allGames.gamesArray[gameIndex].turn, "blue"))
                        yourTurnGames.Add(allGames.gamesArray[gameIndex]);
                    else
                        opponentTurnGames.Add(allGames.gamesArray[gameIndex]);
                }
                else
                {
                    if (System.String.Equals(allGames.gamesArray[gameIndex].turn, "red"))
                        yourTurnGames.Add(allGames.gamesArray[gameIndex]);
                    else
                        opponentTurnGames.Add(allGames.gamesArray[gameIndex]);
                }
            }
        }
        PopulateGamesScroll();
    }

    public void PopulateUI()
    {
        coinsText.text = MainGameManager.Instance.PlayerCoins.ToString();
        userNameText.text = MainGameManager.Instance.LocalPlayerName.ToString();
        gamesPlayedText.text = MainGameManager.Instance.TotalGames.ToString();
        pointsEarnedText.text = MainGameManager.Instance.TotalScore.ToString();
        victoriesText.text = MainGameManager.Instance.GameWins.ToString();
        answerStreakText.text = MainGameManager.Instance.AnswerStreak.ToString();
        //TODO: rankText;
    }

    void PopulateGamesScroll()
    {
        int childCount = contentTrans.childCount;
        for (int i = 0; i < childCount; i++)
        {
            GameObject childObj = contentTrans.GetChild(i).gameObject;
            if (childObj.tag == "listItem")
                GameObject.Destroy(childObj);
        }
        AddGamesToList(yourTurnGames, 1);
        AddGamesToList(opponentTurnGames, 2);
        AddGamesToList(finishedGames, 3);
        statsRect.SetAsLastSibling();
    }

    void AddGamesToList(List<Game> gamesToAdd, int turnType)
    {
        if (gamesToAdd.Count != 0)
        {
            gamesToAdd.Sort();

            GameObject titlePrefab = null;
            switch (turnType)
            {
                case 1:
                    titlePrefab = yourTurnGamesTitle;
                    break;
                case 2:
                    titlePrefab = oppTurnGamesTitle;
                    break;
                case 3:
                    titlePrefab = finishedGamesTitle;
                    if (gamesToAdd.Count > 20) //remove games above 20
                    {
                        Debug.Log("removing more than 20 finished games");
                        for (int indexAbove20 = 0; indexAbove20 < gamesToAdd.Count - 20; indexAbove20++)
                        {
                            userService.RemoveFinishedGame(gamesToAdd[indexAbove20 + 20]);
                        }
                        gamesToAdd.RemoveRange(20, gamesToAdd.Count - 20);
                    }
                    break;

            }
            GameObject yourTurnTitle = Instantiate(titlePrefab) as GameObject;
            yourTurnTitle.transform.SetParent(contentTrans);
            yourTurnTitle.transform.localScale = Vector3.one;
            if (turnType == 3)
                yourTurnTitle.GetComponentInChildren<Button>().onClick.AddListener(() => RemoveFinishedGames());
            foreach (Game yourTurnGame in gamesToAdd)
            {
                GameObject yourTurnItem = Instantiate(activeGameList) as GameObject;
                GameListItemController listControl = yourTurnItem.GetComponent<GameListItemController>();
                listControl.gameId = yourTurnGame.gameId;
                listControl.viewPort = viewPort;
                string opponentId = "";
                string opponentName = "";
                string opponentPhotoLink = "";
                if (System.String.Equals(yourTurnGame.redPlayerId, MainGameManager.Instance.LocalPlayerId))
                {
                    listControl.opponentName.text = yourTurnGame.bluePlayerName;
                    opponentId = yourTurnGame.bluePlayerId;
                    opponentName = yourTurnGame.bluePlayerName;
                    opponentPhotoLink = yourTurnGame.bluePlayerPhotoLink;
                    //StartCoroutine(SetUserPhoto(listControl.playerPhoto, yourTurnGame.bluePlayerPhotoLink));
                    listControl.photoUrl = yourTurnGame.bluePlayerPhotoLink;
//                    if (!PhotosLoader.Instance.photosDictionary.ContainsKey(yourTurnGame.bluePlayerPhotoLink))
//                        PhotosLoader.Instance.AddPhotoByUrl(yourTurnGame.bluePlayerPhotoLink);
                    listControl.SetOutline(false);
                }
                else
                {
                    listControl.opponentName.text = yourTurnGame.redPlayerName;
                    opponentId = yourTurnGame.redPlayerId;
                    opponentName = yourTurnGame.redPlayerName;
                    opponentPhotoLink = yourTurnGame.redPlayerPhotoLink;
                    //StartCoroutine(SetUserPhoto(listControl.playerPhoto, yourTurnGame.redPlayerPhotoLink)); 
                    listControl.photoUrl = yourTurnGame.redPlayerPhotoLink;
//                    if (!PhotosLoader.Instance.photosDictionary.ContainsKey(yourTurnGame.redPlayerPhotoLink))
//                        PhotosLoader.Instance.AddPhotoByUrl(yourTurnGame.redPlayerPhotoLink);
                    listControl.SetOutline(true);
                }
                if (turnType == 3)
                {
                    listControl.playButton.SetActive(false);
                    listControl.rematchButton.SetActive(true);
                    listControl.rematchButton.GetComponent<Button>().onClick.AddListener(() => SaveOpponentPhotoLink(opponentPhotoLink));
                    listControl.rematchButton.GetComponent<Button>().onClick.AddListener(() => SaveOpponentName(opponentName));
                    listControl.rematchButton.GetComponent<Button>().onClick.AddListener(() => Rematch(opponentId));
                    //check to remove button above
                    listControl.lastMoveText.text = "Finished - " + AgoTimeSpan.Instance.GetRightTimeSpanValue(yourTurnGame.lastTurn);
                    if (yourTurnGame.redWins && System.String.Equals(yourTurnGame.redPlayerId, MainGameManager.Instance.LocalPlayerId))
                    {
                        listControl.roundNoText.text = "YOU WON!";
                    }
                    else
                    {
                        listControl.roundNoText.color = new Color(205 / 255.0f, 160 / 255.0f, 39 / 255.0f);
                        listControl.roundNoText.text = "YOU LOST!";
                    }
                }
                else
                {
                    listControl.roundNoText.text = "Round " + yourTurnGame.roundNo.ToString();
                    listControl.lastMoveText.text = "Last move - " + AgoTimeSpan.Instance.GetRightTimeSpanValue(yourTurnGame.lastTurn);
                    if (turnType == 1)
                        listControl.canPlay = true;
                }
                listControl.fieldScript.PopulateField(yourTurnGame.cellValues);
                yourTurnItem.transform.SetParent(contentTrans);
                yourTurnItem.transform.localScale = Vector3.one;
            }
        }
    }

    IEnumerator SetUserPhoto(Image photoImage, string photoURL)
    {
        if (photoURL == " ")
            yield break;
        
        WWW www = new WWW(photoURL);
        yield return www; 

        Sprite sprite = new Sprite();
        sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        photoImage.sprite = sprite;
    }

    public static Game getGameById(string gameId)
    {
        foreach (Game yourTurnGame in yourTurnGames)
        {
            if (yourTurnGame.gameId == gameId)
                return yourTurnGame;
        }
        return null;
    }

    public void PlayGame()
    {
        SessionHandler.Instance.currentSession = null;
        SceneManager.LoadScene("GameField");
    }

    public void RemoveFinishedGames()
    {
        foreach (Game finishedGame in finishedGames)
        {
            userService.RemoveFinishedGame(finishedGame);
        }
    }

    public void SaveOpponentName(string opponentName)
    {
        MainGameManager.Instance.OpponentName = opponentName;
    }

    public void SaveOpponentPhotoLink(string opponentPhotoLink)
    {
        MainGameManager.Instance.OpponentPhotoLink = opponentPhotoLink;
    }

    public void Rematch(string opponentId)
    {
        //TODO: check if not playing with opponent now
        MainGameManager.Instance.OpponentId = opponentId;
        SessionHandler.Instance.currentSession = null;
        SceneManager.LoadScene("GameField");
    }

    void OnApplicationPause(bool pause)
    {
        if (pause) // we are in background
        {
            Debug.Log("paused");
        }
        else // we are in foreground again.
        {
            Debug.Log("unpaused");
            AdMobAds.Instance.ShowInterstatial();
        }
    }
}
